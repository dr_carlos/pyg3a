#!/usr/bin/env python3

import fxcg

type ExampleList[T] = list[T]

str_list: ExampleList[str] = ["hello", "bye", "hi"]
# hello, bye, hi

str_list.append("goodbye")
# hello, bye, hi, goodbye

if "goodbye" in str_list:
    str_list.insert(1, "hello")
# hello, hello, bye, hi, goodbye

str_list.pop(3)
# hello, hello, bye, goodbye

del str_list[0]
# hello, bye, goodbye

str_list_2: list[str] = ["no way"]
str_list += str_list_2
# hello, bye, goodbye, no way

for i, string in enumerate(str_list):
    PrintXY(1, i + 1, string)

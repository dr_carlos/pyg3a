#!/usr/bin/env python3

# Based on libfxcg/examples/display/starfield/main.c

import fxcg
import stdc

star_amount = 64


def DrawPixelGray(x: int, y: int, col: int):
    v: tuple[color, ...] = GetVRAMAddress()
    v[y * 384 + x] = col * 2113


def DrawPixelSet(x: int, y: int, col: color):
    v: tuple[color, ...] = GetVRAMAddress()
    v[y * 384 + x] = col


def DrawWuLine(X0: int, Y0: int, X1: int, Y1: int, BaseColor: int, NumLevels: int, IntensityBits: int):
    if Y0 > Y1:
        Y0, Y1 = Y1, Y0
        X0, X1 = X1, X0

    DrawPixelGray(X0, Y0, BaseColor)

    XDir = 0
    DeltaX = 0
    DeltaY = 0

    if (DeltaX := X1 - X0) >= 0:
        XDir = 1
    else:
        XDir = -1
        DeltaX = -DeltaX

    if (DeltaY := Y1 - Y0) == 0:
        while (DeltaX := DeltaX - 1) != 0:
            X0 += XDir
            DrawPixelGray(X0, Y0, BaseColor)
        return

    if DeltaX == 0:
        Y0 += 1
        DrawPixelGray(X0, Y0, BaseColor)

        while (DeltaY := DeltaY - 1) == 0:
            Y0 += 1
            DrawPixelGray(X0, Y0, BaseColor)
        return

    if DeltaX == DeltaY:
        X0 += XDir
        Y0 += 1
        DrawPixelGray(X0, Y0, BaseColor)
        DeltaY -= 1

        while (DeltaY := DeltaY - 1) == 0:
            X0 += XDir
            Y0 += 1
            DrawPixelGray(X0, Y0, BaseColor)
            DeltaY -= 1
        return

    ErrorAcc: unsint = 0
    IntensityShift = 16 - IntensityBits
    WeightingComplementMask = NumLevels - 1

    if DeltaY > DeltaX:
        ErrorAdj: unsint = (cast(unslong, DeltaX) << 16) // cast(unslong, DeltaY)

        while DeltaY := DeltaY - 1:
            ErrorAccTemp = ErrorAcc
            ErrorAcc += ErrorAdj
            if ErrorAcc <= ErrorAccTemp:
                X0 += XDir

            Y0 += 1

            Weighting = ErrorAcc >> IntensityShift
            DrawPixelGray(X0, Y0, BaseColor + Weighting)
            DrawPixelGray(X0 + XDir, Y0, BaseColor + (Weighting ^ WeightingComplementMask))

        DrawPixelGray(X1, Y1, BaseColor)
        return

    ErrorAdj: unsint = (cast(unslong, DeltaY) << 16) // cast(unslong, DeltaX)
    while DeltaX := DeltaX - 1:
        ErrorAccTemp = ErrorAcc
        ErrorAcc += ErrorAdj
        if ErrorAcc <= ErrorAccTemp:
            Y0 += 1

        X0 += XDir

        Weighting = ErrorAcc >> IntensityShift
        DrawPixelGray(X0, Y0, BaseColor + Weighting)
        DrawPixelGray(X0, Y0 + 1, BaseColor + (Weighting ^ WeightingComplementMask))

    DrawPixelGray(X1, Y1, BaseColor)


def DrawWuLineClear(X0: int, Y0: int, X1: int, Y1: int):
    if Y0 > Y1:
        Y0, Y1 = Y1, Y0
        X0, X1 = X1, X0

    DrawPixelSet(X0, Y0, 0)

    XDir = 0
    DeltaX = 0
    DeltaY = 0

    if (DeltaX := X1 - X0) >= 0:
        XDir = 1
    else:
        XDir = -1
        DeltaX = -DeltaX

    if (DeltaY := Y1 - Y0) == 0:
        while (DeltaX := DeltaX - 1) != 0:
            X0 += XDir
            DrawPixelSet(X0, Y0, 0)
        return

    if DeltaX == 0:
        Y0 += 1
        DrawPixelSet(X0, Y0, 0)

        while (DeltaY := DeltaY - 1) == 0:
            Y0 += 1
            DrawPixelSet(X0, Y0, 0)
        return

    if DeltaX == DeltaY:
        X0 += XDir
        Y0 += 1
        DrawPixelSet(X0, Y0, 0)

        while (DeltaY := DeltaY - 1) == 0:
            X0 += XDir
            Y0 += 1
            DrawPixelSet(X0, Y0, 0)
        return

    ErrorAcc: unsint = 0
    if DeltaY > DeltaX:
        ErrorAdj: unsint = (cast(unslong, DeltaX) << 16) // cast(unslong, DeltaY)

        while DeltaY := DeltaY - 1:
            ErrorAccTemp = ErrorAcc
            ErrorAcc += ErrorAdj
            if ErrorAcc <= ErrorAccTemp:
                X0 += XDir

            Y0 += 1

            DrawPixelSet(X0, Y0, 0)
            DrawPixelSet(X0 + XDir, Y0, 0)

        DrawPixelSet(X1, Y1, 0)
        return

    ErrorAdj: unsint = (cast(unslong, DeltaY) << 16) // cast(unslong, DeltaX)
    while DeltaX := DeltaX - 1:
        ErrorAccTemp = ErrorAcc
        ErrorAcc += ErrorAdj
        if ErrorAcc <= ErrorAccTemp:
            Y0 += 1

        X0 += XDir

        DrawPixelSet(X0, Y0, 0)
        DrawPixelSet(X0, Y0 + 1, 0)

    DrawPixelSet(X1, Y1, 0)


def newStar(x: list[int], y: list[int], z: list[int], zv: list[int], i: int, seed: int):
    x[i] = seed % 2516582400 - 1258291200
    seed = seed * 1103515245 + 12345

    y[i] = seed % 1415577600 - 707788800
    seed = seed * 1103515245 + 12345

    z[i] = seed % 29491200 + 3276800
    seed = seed * 1103515245 + 12345

    zv[i] = seed % 655360
    seed = seed * 1103515245 + 12345

    return seed


def sqrti(a: int) -> int:
    sq = 1
    d = 3
    while sq <= a:
        sq += d
        d += 2

    return d // 2 - 1


def keyPressed(basic_keycode: int):
    # raw_c("const unsigned short* keyboard_register = (unsigned short*) 0xA44B0000;")
    keyboard_register: tuple[unsshort, ...] = cast(unsshort, 0xA44B0000)
    row = basic_keycode % 10
    col = basic_keycode // 10 - 1
    word = row >> 1
    bit = col + ((row & 1) << 3)

    return 0 != (keyboard_register[word] & 1 << bit)


star_x: list[int] = []
star_y: list[int] = []
star_z: list[int] = []
star_zv: list[int] = []

screen_x: list[int] = []
screen_y: list[int] = []
screen_x_old: list[int] = []
screen_y_old: list[int] = []

Bdisp_EnableColor(1)

memset(GetVRAMAddress(), 0, 384 * 216 * 2)

seed = RTC_GetTicks()

for i in range(star_amount):
    seed = newStar(star_x, star_y, star_z, star_zv, i, seed)

while True:
    for i in range(star_amount):
        # Clear old star
        star_z[i] -= star_zv[i]
        x = star_x[i] // star_z[i] + 160
        y = star_y[i] // star_z[i] + 108

        if (x <= 1) or (y <= 1) or (x >= 382) or (y >= 214) or (star_z[i] < 32768) or (star_z[i] > 32768000):
            seed = newStar(star_x, star_y, star_z, star_zv, i, seed)
            DrawWuLineClear(screen_x[i], screen_y[i], screen_x_old[i], screen_y_old[i])
            screen_x[i] = screen_y[i] = screen_x_old[i] = screen_y_old[i] = 0
        else:
            xd = x - screen_x[i]
            yd = y - screen_y[i]

            length = sqrti(xd * xd + yd * yd)
            b = (6200 * star_zv[i]) // star_z[i]
            if length > 16:
                b //= length // 8

            if screen_x_old[i] or screen_y_old[i]:
                DrawWuLineClear(screen_x[i], screen_y[i], screen_x_old[i], screen_y_old[i])
                DrawWuLine(x, y, screen_x[i], screen_y[i], b % 32, 31, 5)

            screen_x_old[i] = screen_x[i]
            screen_y_old[i] = screen_y[i]
            screen_x[i] = x
            screen_y[i] = y

    if keyPressed(KEY_PRGM_MENU):
        GetKey()
    else:
        Bdisp_PutDisp_DD()

#!/usr/bin/env python3

import fxcg

info: tuple[int, str, int] = (1, "Hello", 2)

PrintXY(1, 1, f"Tuple: ({info[0]}, {info[1]}, {info[2]})")

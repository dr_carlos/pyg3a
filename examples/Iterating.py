#!/usr/bin/env python3

import fxcg

l: list[int] = [0, 1]

for i in range(len(l), len(range(7)), len([0, 1])):
    PrintXY(i, i, "Hi")
    GetKey()

Bdisp_AllClr_VRAM()

for i, s in enumerate(["Hello", "World"]):
    PrintXY(i + 1, i + 1, s)
    GetKey()

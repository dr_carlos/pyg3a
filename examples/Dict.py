import fxcg

d: dict[str, str] = {"a": "HELLO", "b": "HI", "c": "HOLA"}

d["a"] = "goodbye"

if "c" in d:
    d["b"] = "Bonjour"

PrintXY(1, 4, d["a"])
PrintXY(1, 5, d["b"])
PrintXY(1, 6, d["c"])

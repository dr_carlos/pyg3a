import random

for _ in range(5):
    print(randint(1, 10), end=" ")
print()

for _ in range(5):
    print(randrange(5), end=" ")
print()

for _ in range(5):
    print(randrange(1, 10, 2), end=" ")
print()

for _ in range(4):
    print(choice(["Hello", "Hi", "Goodbye", "Bonjour", "Au Revoir"]), end=" ")
print()

for _ in range(4):
    print(choice(["Hello", "Hi", "Goodbye", "Bonjour", "Au Revoir"]), end=" ")

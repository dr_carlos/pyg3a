#!/usr/bin/env python3

import fxcg

TOTAL_MESSAGES: int = 3356
MESSAGES_PER_PAGE: int = 12


def msgStatus(msg: str):
    EnableStatusArea(2)
    DefineStatusAreaFlags(3, SAF_BATTERY | SAF_TEXT, 0, 0)
    DefineStatusMessage(msg, TextColor.BLACK)
    DisplayStatusArea()


def printMiniMsgNo(y: int, msgno: int):
    cursor: int
    cursor, _ = PrintMini(
        0, y, LocalizeMessage(msgno), PrintMiniMode.STATUS_AREA, color=COLOR_WHITE, back_color=COLOR_BLUE
    )

    # If message is not empty
    if cursor != 0:
        PrintMini(
            cursor,
            y,
            f" #{msgno}",
            PrintMiniMode.TRANSPARENT_BACKGROUND | PrintMiniMode.STATUS_AREA,
        )


language = LocalizeMessage(0)  # English (or French, etc.)
is_chinese = language == "Chinese"

if is_chinese:
    MESSAGES_PER_PAGE = 8
    msgStatus(r"Any Key : Next, /\\ : Prev or < > : Skip")

Bdisp_EnableColor(1)
Bdisp_PutDisp_DD()

FrameColor(1, COLOR_GREEN)

current_message_no = TOTAL_MESSAGES
while True:
    # Wrap from jumping
    if current_message_no < 0 or current_message_no > TOTAL_MESSAGES:
        current_message_no = TOTAL_MESSAGES

    # Initial
    if current_message_no == TOTAL_MESSAGES:
        Bdisp_AllClr_VRAM()
        SetBackGround(8)  # Language background

        PrintXY_2(5, 0 if is_chinese else -1, 0, 1, TextColor.BLUE)  # English (or French, etc.)
        if is_chinese:
            PrintXY(6, 1, "#", TextMode.TRANSPARENT_BACKGROUND)
            PrintXY_2(5, 5, 0, 647)
            PrintXY_2(5, 7, 7, 813)
            PrintXY(16, 8, ":")
            PrintXY_2(5, 12, 7, 292)

        else:
            msgStatus("Any Key : Next, \xE6\x9C : Prev or \xE6\x9A \xE6\x9B : Skip")
            EnableStatusArea(0)

            language_name_in_locale = LocalizeMessage(1)  # English (or Francais, etc.)
            locate_OS(len(language_name_in_locale) + 1, 1)

            Print_OS(f"#{LocalizeMessage(647)}", TextMode.TRANSPARENT_BACKGROUND)  # #Message

            any_key = LocalizeMessage(813)  # Any Key
            locate_OS(21 - len(any_key), 7)
            Print_OS(f"{any_key}:", TextMode.TRANSPARENT_BACKGROUND)  # Any Key:

            next_page = LocalizeMessage(292)  # Next Page
            locate_OS(21 - len(next_page), 8)
            Print_OS(next_page, TextMode.TRANSPARENT_BACKGROUND)  # Next Page

        PrintMini(
            230,
            48,
            language,
            PrintMiniMode.TRANSPARENT_BACKGROUND,
            color=COLOR_BLUE,
        )  # English (or French, etc.)

        PrintXY(19, 4, "\xE6\x9C", TextMode.TRANSPARENT_BACKGROUND)  # Up arrow
        PrintXY(17, 5, "\xE6\x9A   \xE6\x9B", TextMode.TRANSPARENT_BACKGROUND)  # Left + Right arrows
        PrintXY(19, 6, "\xE6\x9D", TextMode.TRANSPARENT_BACKGROUND)  # Down arrow

        PrintMini(309, 99, "-10+", PrintMiniMode.TRANSPARENT_BACKGROUND)  # key inside arrows

    elif is_chinese:
        Bdisp_Fill_VRAM(COLOR_WHITE, 1)

        for y in range(MESSAGES_PER_PAGE):
            message_length = len(LocalizeMessage(current_message_no))

            if message_length:
                PrintXY_2(5, 0, y, current_message_no, TextColor.BLUE)
                message = str(current_message_no)
                x = 22 - len(message)

                if (x - message_length) > 1:
                    locate_OS(message_length + 2, y + 1)
                else:
                    locate_OS(x, y + 1)

                Print_OS(message, TextMode.TRANSPARENT_BACKGROUND)

            current_message_no += 1

    else:
        Bdisp_AllClr_VRAM()
        EnableStatusArea(3)

        y = 1
        for x in range(MESSAGES_PER_PAGE):
            printMiniMsgNo(y, current_message_no)
            current_message_no += 1
            y += 18

    key = GetKey()

    # Backwards 1 page
    if key == KEY_CTRL_UP:
        current_message_no -= 2 * MESSAGES_PER_PAGE

    # Forwards 10 pages
    elif key == KEY_CTRL_RIGHT:
        if current_message_no == TOTAL_MESSAGES:
            current_message_no = 10 * MESSAGES_PER_PAGE
        else:
            current_message_no += 9 * MESSAGES_PER_PAGE

    # Backwards 10 pages
    elif key == KEY_CTRL_LEFT:
        current_message_no -= 11 * MESSAGES_PER_PAGE

    # Go to intial page on EXIT
    elif key == KEY_CTRL_EXIT:
        current_message_no = TOTAL_MESSAGES

    # Wrap around
    elif current_message_no == TOTAL_MESSAGES:
        current_message_no = 0

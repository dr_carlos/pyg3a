# /usr/bin/env python3

import fxcg
import stdc

type KeyboardRegister = tuple[unsshort, ...]


def keyPressed(basic_keycode: int):
    keyboard_register: KeyboardRegister = cast(KeyboardRegister, 0xA44B0000)
    row = basic_keycode % 10
    col = basic_keycode // 10 - 1

    return (keyboard_register[row >> 1] & 1 << (col + ((row & 1) << 3))) != 0


Bdisp_EnableColor(0)
Bdisp_AllClr_VRAM()
Bdisp_PutDisp_DD()

while True:
    HourGlass()

    if keyPressed(KEY_PRGM_MENU):
        GetKey()

=====
PyG3A
=====

Description
-----------

A tool to convert Python files to Casio fx-CG 10/20/50 (Prizm) Add-ins

Installation
------------
::

	pip install pyg3a

Usage
-----
::

	pyg3a [--help] [--debug] [--verbose] -l path/to/libfxcg <file.py>

	arguments:
		file.py               name of python file to convert

	options:
		-h, --help            show this help message and exit
		--debug               use debug mode
		--verbose             print command names in make
		-l path/to/libfxcg, --libfxcg path/to/libfxcg
				      libfxcg location



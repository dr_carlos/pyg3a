#!/usr/bin/env fish

for define in (cat ~/pkg/libfxcg-me/include/fxcg/$argv[1].h | grep "#define")
    set split (string split -m 1 " " (string replace -r '#define\s+' '' $define | string replace -ar '\s+' ' '))

    if test (count $split) -eq 2
        set type (string match -r '\(([a-zA-Z_ \*]+)\)' $split[2])[2]
        if test -n "$type"
            echo "'$split[1]'": "\"$type\"",
        else
            echo "'$split[1]'": Types.int,
        end
    end
end
import libcst as cst
import pytest

import pyg3a
from pyg3a.errors import NotAnnotatedError, TranspileWarning
from pyg3a.functions import Function, Parameter
from pyg3a.types import Types
from tests import raises_transpile


def test_func_creation(main):
    func = Function(cst.parse_statement("def func(param: int) -> None: ..."), pyg3a.Main.globs)


def test_func_kwargs_creation(main):
    func = (
        Function(
            cst.parse_statement("pass"),
            pyg3a.Main.globs,
            name="func",
            ret=Types.int,
            node=cst.parse_statement("def a():\n\treturn 2"),
            statements=[
                cst.parse_statement("b: int = 10"),
                cst.parse_statement("call_function(b)"),
                cst.parse_statement("return b"),
            ],
            params=[
                Parameter(
                    name=cst.Name(value="name"),
                    annotation=cst.Annotation(annotation=cst.Name(value="str")),
                    default=None,
                    star="",
                )
            ],
        ).construct(),
    )


def test_missing_annotations(main):
    with raises_transpile(NotAnnotatedError):
        func = Function(cst.parse_statement("def func(param) -> None: ..."), pyg3a.Main.globs)


def test_func_registration(main):
    func = Function(
        cst.parse_statement("def func(param: int, paramb: str, paramc: tuple[int]) -> None: ..."), pyg3a.Main.globs
    )
    assert issubclass(
        pyg3a.Main.globs["func"].type, Types.Callable[[Types.int, Types.str, Types.tuple[Types.int]], Types.NoneType]
    )


def test_func_simple_registration(main):
    func = Function(cst.parse_statement("def func(param: int) -> None: ..."), pyg3a.Main.globs)
    assert issubclass(pyg3a.Main.globs["func"].type, Types.Callable[[Types.int], Types.NoneType])


def test_func_str(main):
    func = Function(cst.parse_statement("def func(param: int) -> str: ..."), pyg3a.Main.globs)
    assert (
        repr(func)
        == """Function(
\tname='func',
\targs=(param: int),
\tstatements=[...],
\tret='String'
)"""
    )


@pytest.mark.parametrize(
    "py_func,c_func",
    [
        (
            "def func(param: int) -> str: a: float = 0.1",
            """static String func(int param) {
\tdouble a = 0.1;
}""",
        ),
        (
            """
def func(param: int, param2: str) -> float: 
    a: float = 0.1
    return a
                """,
            """static double func(int param, String param2) {
\tdouble a = 0.1;
\treturn a;
}""",
        ),
        (
            """
def func(): 
    a: float = 0.1
    return a
                    """,
            """static double func() {
\tdouble a = 0.1;
\treturn a;
}""",
        ),
        (
            """
def func(): 
    a: float = 0.1
    return
                    """,
            """static void func() {
\tdouble a = 0.1;
\treturn;
}""",
        ),
        (
            """
def main() -> int: 
    a: float = 0.1
                    """,
            """int main() {
\tdouble a = 0.1;
\tint __tmp_key_0; while (1) GetKey(&__tmp_key_0);
}""",
        ),
    ],
)
def test_func_construct(main, py_func: str, c_func: str):
    func = Function(cst.parse_statement(py_func), pyg3a.Main.globs)
    assert func.construct() == c_func


def test_func_auto_construct(main):
    func = Function(
        cst.parse_statement(
            """
def func(): 
    return call()
    """
        ),
        pyg3a.Main.globs,
    )
    with pytest.warns(TranspileWarning, match="Return type"):
        assert (
            func.construct()
            == """static auto func() {
\treturn call();
}"""
        )


def test_declaration(main):
    func = Function(cst.parse_statement("def func(param: int, param2: str) -> float: ..."), pyg3a.Main.globs)
    assert func.declaration() == "static double func(int param, String param2);"

from pathlib import Path

import pyg3a

import pytest


@pytest.fixture(scope="function")
def main(request):
    new_main = pyg3a.pyg3a.MainSingleton()
    for k, v in new_main.__dict__.items():
        setattr(pyg3a.pyg3a.Main, k, v)

    pyg3a.pyg3a.Main.libfxcg = Path("~/pkg/libfxcg").expanduser().absolute()

    def clear_main() -> None:
        for key in new_main.__dict__:
            delattr(pyg3a.pyg3a.Main, key)

    request.addfinalizer(clear_main)
    return None

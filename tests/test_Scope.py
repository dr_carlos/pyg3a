from typing import cast

import libcst as cst
import pytest

from pyg3a.scope import Scope, VariableInfo, NoValue
from pyg3a.types import Types


def test_kw_creation():
    Scope(a=VariableInfo(Types.int, NoValue), b=(Types.str, NoValue))


def test_dict_creation():
    Scope(None, {"a": VariableInfo(Types.int, NoValue), "b": (Types.str, NoValue)})


def test_kw_and_dict():
    dict_scope = Scope(None, {"a": VariableInfo(Types.int, NoValue), "b": (Types.str, NoValue)})
    kw_scope = Scope(a=(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    assert dict_scope["a"] == kw_scope["a"]
    assert dict_scope["b"] == kw_scope["b"]


def test_set_type():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    scope.set_type(cst.parse_expression("a"), Types.str)
    assert scope["a"].type is Types.str


def test_get_type():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    scope.set_type(cst.parse_expression("a"), Types.str)
    with pytest.raises(KeyError):
        print(scope["c"])


def test_new_type():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    scope.set_type(cst.parse_expression("c"), Types.str)
    assert scope["c"].type is Types.str


def test_set_value():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    scope.set_value(cst.parse_expression("a"), 2)
    assert scope["a"].value == 2


def test_new_value():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope.set_value(cst.parse_expression("c"), 2)


def test_wrong_name_set_value():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope.set_value(cst.Integer(value="2"), Types.float)


def test_failed_set_type():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope.set_type(cst.Name(), Types.str)


def test_wrong_name_set_var():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope.set_var(cst.Integer(value="2"), Types.float, 0.0)


def test_wrong_type_set_var():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope.set_var(cst.Name(value="name"), float, 0.0)


def test_set_var():
    scope = Scope(a=(Types.int, NoValue), b=(Types.str, NoValue))
    scope.set_var(cst.parse_expression("b"), Types.object, 10)
    assert scope["b"].type == Types.object
    assert scope["b"].value == 10


def test_wrong_name_set_type():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope.set_type(cst.Integer(value="2"), Types.float)


def test_wrong_type_set_type():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope.set_type(cst.Name(value="name"), float)


def test_setitem():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope["a"] = Types.str

    assert scope["a"].type is Types.int


def test_empty_name_set_func():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope.set_func("", [Types.int], Types.int)


def test_invalid_param_types_set_func():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope.set_func("func", [int], Types.int)


def test_invalid_return_type_set_func():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    with pytest.raises(TypeError):
        scope.set_func("func", [Types.int], int)


def test_contains():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    assert "a" in scope


def test_cst_contains():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    assert cst.parse_expression("a") in scope


def test_str_inner():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    inner = scope.inner("c", Types.str)
    assert inner["c"].type is Types.str
    assert "c" not in scope


def test_param_inner():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    inner = scope.inner(cast(cst.FunctionDef, cst.parse_statement("def func(c: str): ...")).params.params[0], Types.str)
    assert inner["c"].type is Types.str
    assert "c" not in scope


def test_param_edit_inner():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    inner = scope.inner(cast(cst.FunctionDef, cst.parse_statement("def func(a: str): ...")).params.params[0], Types.str)
    assert inner["a"].type is Types.str
    assert scope["a"].type is Types.int


def test_str_edit_inner():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    inner = scope.inner("a", Types.str)
    assert inner["a"].type is Types.str
    assert scope["a"].type is Types.int


def test_str_mult_inner():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    inner = scope.inner(["c", "d"], [Types.str, Types.float])
    assert inner["c"].type is Types.str
    assert inner["d"].type is Types.float
    assert "c" not in scope
    assert "d" not in scope


def test_str_mult_edit_inner():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    inner = scope.inner(["a", "b"], [Types.str, Types.float])
    assert inner["a"].type is Types.str
    assert inner["b"].type is Types.float
    assert scope["a"].type is Types.int
    assert scope["b"].type is Types.str


def test_param_mult_inner():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    inner = scope.inner(
        cast(cst.FunctionDef, cst.parse_statement("def func(a: str, b: float): ...")).params.params,
        [Types.str, Types.float],
    )
    assert inner["a"].type is Types.str
    assert inner["b"].type is Types.float
    assert scope["a"].type is Types.int
    assert scope["b"].type is Types.str


def test_blank_inner():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    inner = scope.inner()
    assert scope["a"] == inner["a"]
    assert scope["b"] == inner["b"]


def test_copy_inner():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue)).copy()
    inner = scope.inner()
    assert scope["a"] == inner["a"]
    assert scope["b"] == inner["b"]


def test_copy():
    scope = Scope(a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))
    copied = scope.copy()
    scope.set_type(cst.parse_expression("a"), Types.str)
    assert copied["a"].type is Types.int


def test_repr():
    assert (
        repr(Scope(Scope(), a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue)))
        == "Scope(parent=Scope(parent=None, mapping={}), mapping={'a': VariableInfo(type=<class 'pyg3a.types.numbers.CInt'>, value=NoValue), 'b': VariableInfo(type=<class 'pyg3a.types.string.CStr'>, value=NoValue)})"
    )


def test_str():
    assert str(Scope(Scope(), a=VariableInfo(Types.int, NoValue), b=VariableInfo(Types.str, NoValue))) == (
        """Scope(
      parent=Scope(
            parent=None,
            <no data>
      ),
      a: (<class 'pyg3a.types.numbers.CInt'>, NoValue), 
      b: (<class 'pyg3a.types.string.CStr'>, NoValue)
)"""
    )


def test_iter():
    for name, info in Scope(a=(Types.int, NoValue), b=(Types.str, NoValue)):
        assert name in ("a", "b")
        assert info.type is Types.int if name == "a" else Types.str

from pathlib import Path

import pytest

import pyg3a
from pyg3a.modules import Module


def gen_module(text: str = "", name: str = "__tmp") -> Module:
    if text:
        return Module(name).parse_text(text)

    # Find module_name.py in Main.package_locs
    # Final location has priority

    file_path: Path = Path()
    for loc in pyg3a.Main.package_locs:
        if loc.joinpath(f"{name}.py").is_file():
            file_path = loc.joinpath(f"{name}.py")

    return Module(name).parse_file(file_path)


def test_classdef(main):
    gen_module("class A:\n\tpass")


def test_unsupported(main):
    with pytest.raises(SyntaxError):
        gen_module("if 1 == 1:\n\tpass")
    with pytest.raises(SyntaxError):
        gen_module("1 == 1")


def test_imports(main):
    includes: set[str] = pyg3a.Main.project.includes.copy()
    gen_module("import fxcg.display, stdint, list")
    assert pyg3a.Main.project.includes - includes == {"fxcg/display.h", "stdint.h", "list.hpp"}


def test_importfrom(main):
    includes: set[str] = pyg3a.Main.project.includes.copy()
    gen_module("from fxcg.display import *")
    assert pyg3a.Main.project.includes - includes == {"fxcg/display.h"}

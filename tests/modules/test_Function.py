import libcst as cst

from pyg3a.errors import NotAnnotatedError
from pyg3a.modules.functions import Function
from tests import raises_transpile


def test_function_call_create(main):
    f: Function[cst.Call] = Function(
        cst.parse_statement(
            """
def range(stop: int) -> tuple[int, ...]:
    return f"{{{', '.join([str(i) for i in range(int(stop))])}}}"
"""
        ),
        "__test",
        cst.Call,
    )


def test_function_for_create(main):
    f: Function[cst.For] = Function(
        cst.parse_statement(
            """
def range__iter__(var_name: str, stop: int) -> int:
    return f"int {var_name} = 0; {var_name} < {stop}; {var_name}++"
"""
        ),
        "__test",
        cst.For,
    )


def test_function_for_create_posonly_params(main):
    f: Function[cst.For] = Function(
        cst.parse_statement(
            """
def func(var_name: str, a: int, /) -> int:
    return var_name
"""
        ),
        "__test",
        cst.For,
    )


def test_function_for_create_kwonly_params(main):
    with raises_transpile(TypeError):
        f: Function[cst.For] = Function(
            cst.parse_statement(
                """
def func(*, a: int) -> int:
    return a
"""
            ),
            "__test",
            cst.For,
        )


def test_function_no_return_annotation():
    with raises_transpile(NotAnnotatedError):
        f: Function[cst.Call] = Function(
            cst.parse_statement(
                """
def func():
    return ""
"""
            ),
            "__test",
            cst.Call,
        )

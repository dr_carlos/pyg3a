import logging

import libcst as cst
import pytest, pyg3a

from pyg3a.errors import CTypeNotConcreteError
from pyg3a.node import node_to_code, node_type, node_to_c_str
from pyg3a.scope import Scope, NoValue
from pyg3a.types import Types
from pyg3a import PyG3A
from tests import raises_transpile


@pytest.mark.parametrize(
    "node,typ",
    [
        ("'Hello'", Types.str),
        ("'con' 'cat'", Types.str),
        ("True", Types.bool),
        ("None", Types.NoneType),
        ("False", Types.bool),
        ("1", Types.int),
        ("1.0", Types.float),
        ("...", Types.EllipsisType),
        ("cast(str, 1)", Types.str),
        ("lambda x,y: None", Types.Callable[[Types.Any, Types.Any], Types.NoneType]),
        ("(1, 2, 3)", Types.tuple[Types.int, Types.int, Types.int]),
        ("[1, 2, 3]", Types.list[Types.int]),
        ("'hi' + 2", Types.NotImplemented),
        ("1j", Types.NotImplemented),
        ("f'{1}a'", Types.str),
        ("f'{1}a' 'b'", Types.str),
        ("'a' 'b' f'a{1}'", Types.str),
        ("str(1)", Types.str),
        ("TextColor.BLACK", Types.int),
        ("0.0193 * 1", Types.float),
        ("0 + 1", Types.int),
        ("0 - 1", Types.int),
        ("0 / 1", Types.float),
        ("0 // 1", Types.int),
        ("10 << 1", Types.int),
        ("10 >> 1", Types.int),
        ("10 & 1", Types.int),
        ("10 | 1", Types.int),
        ("10 ^ 1", Types.int),
        ("10 % 2", Types.int),
        ("10 ** 2", Types.int),
        ("True == False", Types.bool),
        ("1 != 10.0", Types.bool),
        ("'a' < 'b'", Types.bool),
        ("0.1 <= 2.0", Types.bool),
        ("5.0 > 2.0", Types.bool),
        ("5.0 >= 2.0", Types.bool),
        ("1 is 1", Types.bool),
    ],
)
def test_simple_node_type(main, node: str, typ: Types.type):
    PyG3A.import_module("stdpy")
    PyG3A.import_module("fxcg")
    assert issubclass(node_type(cst.parse_expression(node), pyg3a.Main.globs), typ)


@pytest.mark.parametrize(
    "node,typ",
    [
        ("s", Types.str),
        ("b", Types.bool),
        ("i", Types.int),
        ("f", Types.float),
        ("cast(str, i)", Types.str),
        ("(i, i, i)", Types.tuple[Types.int, Types.int, Types.int]),
        ("[i, i, i]", Types.list[Types.int]),
        ("s + i", Types.NotImplemented),
        ("l + i", Types.NotImplemented),
        ("f * i", Types.float),
        ("i + i", Types.int),
        ("i / i", Types.float),
        ("i // i", Types.int),
        ("ls[i]", Types.str),
        ("i in ls", Types.bool),
        ("i << i", Types.int),
    ],
)
def test_scoped_node_type(main, node: str, typ: Types.type):
    assert issubclass(
        node_type(
            cst.parse_expression(node),
            pyg3a.Main.globs.inner(
                ("i", "s", "b", "f", "ls", "l"),
                (
                    Types.int,
                    Types.str,
                    Types.bool,
                    Types.float,
                    Types.list[Types.str],
                    Types.list,
                ),
            ),
        ),
        typ,
    )


def test_out_of_scope_node_type(main):
    with raises_transpile(RuntimeError):
        node_type(cst.parse_expression("ls[1]"), pyg3a.Main.globs)


def test_auto_node_type(main):
    assert issubclass(
        node_type(
            cst.BinaryOperation(left=cst.Integer("0"), operator=cst.Name("o"), right=cst.Integer("1")), pyg3a.Main.globs
        ),
        Types.NotImplemented,
    )
    assert issubclass(
        node_type(
            cst.Comparison(
                left=cst.Integer("0"),
                comparisons=[cst.ComparisonTarget(operator=cst.Name("o"), comparator=cst.Integer("1"))],
            ),
            pyg3a.Main.globs,
        ),
        Types.NotImplemented,
    )


@pytest.mark.parametrize(
    "node,typ",
    [
        ("fun(k, y)", Types.str),
        ("fun(k, y) + fun(k, y)", Types.str),
        ("fun", Types.Callable[[Types.int, Types.float], Types.str]),
    ],
)
def test_func_node_type(main, node: str, typ: Types.type):
    pyg3a.Main.globs.set_func("fun", [Types.int, Types.float], Types.str)
    assert issubclass(node_type(cst.parse_expression(node), pyg3a.Main.globs), typ)


def test_not_concrete_node_type(main):
    with raises_transpile(CTypeNotConcreteError):
        node_type(cst.parse_expression("lambda x: list"), pyg3a.Main.globs)


def test_node_to_code(main):
    assert node_to_code(cst.parse_expression("fun        (  k,   y)")) == "fun        (  k,   y)"


@pytest.mark.parametrize(
    "node,c_str",
    [
        ("'Hello'", 'String("Hello")'),
        ("'con' 'cat'", 'String("concat")'),
        ("True", "1"),
        ("None", "NULL"),
        ("False", "0"),
        ("1", "1"),
        ("1.0", "1.0"),
        ("...", "/* ... */"),
        ("cast(str, 1)", "(String) (1)"),
        ("lambda x,y: None", "[](auto x, auto y){return NULL;}"),
        ("(1, 2, 3)", "{1, 2, 3}"),
        ("[1, 2, 3]", "List<int>{1, 2, 3}"),
        ("[1, 2, 3][0]", "List<int>{1, 2, 3}[0]"),
        ("str(1)", "__stdpystr_str(1)"),
        ("TextColor.BLACK", "TEXT_COLOR_BLACK"),
        ("struct.prop", "struct.prop"),
        ("0.0193 * 1", "(0.0193 * 1)"),
        ("0 + 1", "(0 + 1)"),
        ("0 // 1", "(0 / 1)"),
        ("0.0 // 1.0", "(double) ((int) (0.0 / 1.0))"),
        ("0 and 1", "(0 && 1)"),
        ("-1", "(- 1)"),
        ("10 << 1", "(10 << 1)"),
        ("10 ** 2", "pow(10, 2)"),
        ("True == False", "(1 == 0)"),
        ("1 != 10.0", "(1 != 10.0)"),
        ("'a' < 'b'", '(String("a") < String("b"))'),
        ("0.1 <= 2.0", "(0.1 <= 2.0)"),
        ("1 is 1", "(1 == 1)"),
        ("1 if 1 else 0", "(1 ? 1 : 0)"),
        ("f'{10:.2} {\"Hello\"}'", '_sprintf(String("%.2d %s"), 10, String("Hello").c_str())'),
        (
            "f'{10:.{1 + 1:0}} {\"Hello\"}'",
            '_sprintf(String("%") + String(".") + _sprintf(String("%0d"), (1 + 1)) + String("d") + String(" ") + String("%") + String("s"), 10, String("Hello").c_str())',
        ),
        (
            "f'{10:.{1 + 1:{1 - 1}}} {\"Hello\"}'",
            '_sprintf(String("%") + String(".") + _sprintf(String("%") + _sprintf(String("%d"), (1 - 1)) + String("d"), (1 + 1)) + String("d") + String(" ") + String("%") + String("s"), 10, String("Hello").c_str())',
        ),
    ],
)
def test_simple_node_to_c_str(main, node: str, c_str: str):
    PyG3A.import_module("stdpy")
    PyG3A.import_module("fxcg")
    assert node_to_c_str(cst.parse_expression(node), pyg3a.Main.globs) == c_str


def test_auto_string_node_to_c_str(main, caplog):
    PyG3A.import_module("stdpy")

    caplog.clear()
    with caplog.at_level(logging.WARNING):
        assert (
            node_to_c_str(cst.parse_expression("f'{(1,2)}'"), pyg3a.Main.globs)
            == '_sprintf(String("%s"), String({1, 2}).c_str())'
        )
        assert (
            len(caplog.records) == 1
            and caplog.records[0].msg == "No conversion specifier determined for type, automatically using %s"
        )


@pytest.mark.parametrize(
    "node,c_str",
    [
        ("e.ENUM_MEMBER", "ENUM_MEMBER"),
        ("(i := 2)", "(i = 2)"),
        ("f'{i=}'", '_sprintf(String("i=%d"), i)'),
    ],
)
def test_scoped_node_to_c_str(main, node: str, c_str: str):
    PyG3A.import_module("stdpy")
    assert (
        node_to_c_str(
            cst.parse_expression(node),
            Scope(
                pyg3a.Main.globs,
                {
                    "e": (Types.EnumType, NoValue),
                    "i": (Types.int, 3),
                },
            ),
        )
        == c_str
    )


@pytest.mark.parametrize(
    "node,c_str",
    [
        ("'str'", "str"),
        ("str", "String"),
        ("list[str]", "List<String>"),
    ],
)
def test_type_node_to_c_str(main, node: str, c_str: str):
    assert node_to_c_str(cst.parse_expression(node), pyg3a.Main.globs, is_type=True) == c_str


@pytest.mark.parametrize(
    "node",
    ["sli[1:2:3]", "'s' - 2", "tsi - 3", "(var := 2)", "await x", "f'{0.1}'"],
)
def test_syntax_error_node_to_c_str(main, node: str):
    PyG3A.import_module("stdpy")
    with raises_transpile(SyntaxError):
        node_to_c_str(
            cst.parse_expression(node),
            pyg3a.Main.globs.inner("tsi", Types.tuple[Types.str, Types.int]),
        )

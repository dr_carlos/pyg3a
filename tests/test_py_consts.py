import pytest
import libcst as cst

from pyg3a import pyg3a
from pyg3a.py_consts import node_to_py_const, Constant, py_const_to_c_str
from tests import raises_transpile


@pytest.mark.parametrize(
    "node,const",
    [
        ("...", ...),
        ("19248238", 19248238),
        ("0x182A", 0x182A),
        ("0.192891", 0.192891),
        ("'simple string'", "simple string"),
        ("'concatenated' 'string'", "concatenated" "string"),
        ("'con' 'cat' 'en' 'a' 'ted'", "concatenated"),
        ("True", True),
        ("False", False),
        ("None", None),
    ],
)
def test_node_to_py_const(node: str, const: Constant):
    assert node_to_py_const(cst.parse_expression(node)) == const


@pytest.mark.parametrize(
    "c_str,const",
    [
        ("/* ... */", ...),
        ("19248238", 19248238),
        ("6186", 0x182A),
        ("0.192891", 0.192891),
        ('String("concatenatedstring")', "concatenated" "string"),
        ('String("simple string")', "simple string"),
        ("1", True),
        ("0", False),
        ('{1, String("str")}', (1, "str")),
    ],
)
def test_py_const_to_c_str(c_str: str, const: Constant):
    assert py_const_to_c_str(const) == c_str


def test_null_py_const_to_c_str(main):
    includes: set[str] = pyg3a.Main.project.includes.copy()
    assert py_const_to_c_str(None) == "NULL"
    assert pyg3a.Main.project.includes - includes == {"stddef.h"}


@pytest.mark.parametrize(
    "node,exception", [("f'Hello {world}'", SyntaxError), ("'con' f'cat'", SyntaxError), ("variable", TypeError)]
)
def test_node_to_py_const_error(node: str, exception: type[Exception]):
    with raises_transpile(exception):
        node_to_py_const(cst.parse_expression(node))


@pytest.mark.parametrize("const,exception", [(3j + 2, SyntaxError)])
def test_py_const_to_c_str_error(const: Constant, exception: type[Exception]):
    with pytest.raises(exception):
        py_const_to_c_str(const)

from contextlib import AbstractContextManager
from types import TracebackType
from typing import override

from pyg3a.errors import TranspileError


class TranspileRaisesContext[E: Exception](AbstractContextManager[None]):
    expected_exception: type[E]

    def __init__(self, expected_exception: type[E]) -> None:
        self.expected_exception = expected_exception

    @override
    def __enter__(self) -> None:
        return None

    @override
    def __exit__[
        T: BaseException
    ](self, exc_type: type[T] | None, exc_val: T | None, exc_tb: TracebackType | None,) -> bool:
        assert issubclass(exc_type, TranspileError)
        return isinstance(exc_val.err, self.expected_exception)


def raises_transpile[E: Exception](expected_exception: type[E] | tuple[type[E], ...]) -> TranspileRaisesContext[E]:
    return TranspileRaisesContext(expected_exception)

from typing import cast

import libcst as cst

from pyg3a.errors import NotAnnotatedError
from pyg3a.functions import Parameter
from tests import raises_transpile


def gen_cst_param(param_str: str) -> cst.Param:
    return cast(cst.FunctionDef, cst.parse_statement(f"def func({param_str}): ...")).params.params[0]


def test_param_cst_creation():
    Parameter(gen_cst_param("a: int"))


def test_param_kw_creation():
    Parameter(annotation=cst.Annotation(annotation=cst.Name("int")), name=cst.Name("a"))


def test_param_cst_kw_creation():
    param = Parameter(
        gen_cst_param("a: int"), annotation=cst.Annotation(annotation=cst.Name("str")), name=cst.Name("b")
    )
    assert param.name.value == "b"


def test_unannotated_param_creation():
    with raises_transpile(NotAnnotatedError):
        Parameter(gen_cst_param("a"))

#!/usr/bin/env python3
from dataclasses import dataclass

import libcst as cst


class TranspileError(Exception):
    """
    Wrapper class around an error in the CST when transpiling.
    """

    err: Exception
    node: cst.CSTNode

    def __init__(self, err: Exception | type[Exception], node: cst.CSTNode) -> None:
        self.err = err() if isinstance(err, type) else err
        self.node = node


@dataclass
class TranspileWarning(Warning):
    """
    Warning category when transpiling from Python code.
    (Instantiate this and use it as the message argument to warnings.warn)
    """

    message: str
    node: cst.CSTNode


class AnnotationError(Exception):
    """
    Base class for all errors relating to annotations.
    """

    pass


class NotAnnotatedError(AnnotationError, SyntaxError):
    """
    If a function or variable is missing annotations.
    """

    pass


class CTypeNotConcreteError(NotImplementedError):
    """
    If a variable's type is computed to be used in the C++ output and the type is not concrete or does not exist in C++.
    """

    pass


class FStringTooComplexError(Exception):
    """
    If an f-string is too complex to be converted to C++ without using C++ String concatenation.
    """

    pass

from typing import Any, Callable, override, Self, Iterable, cast

# PyCharm doesn't realise we use this in our exec() in CObjectMeta
# noinspection PyUnresolvedReferences
import pyg3a.types.generics as gens
from pyg3a.errors import CTypeNotConcreteError

_maths_operators: tuple[str, ...] = (
    "add",
    "sub",
    "mul",
    "truediv",
    "floordiv",
    "mod",
    "pow",
    "lshift",
    "rshift",
    "and",
    "xor",
    "or",
)

_unary_maths_operators: tuple[str, ...] = ("neg", "pos", "abs", "invert")

_comparisons: tuple[str, ...] = (
    "le",
    "ge",
    "lt",
    "gt",
    "eq",
    "ne",
)

_container_operators: tuple[str, ...] = ("getitem", "delitem", "contains")

_mutable_container_operators: tuple[str, ...] = ("setitem",)

all_operators: tuple[str, ...] = (
    _maths_operators
    + _unary_maths_operators
    + _comparisons
    + _container_operators
    + _mutable_container_operators
    + ("subclasscheck", "str", "bool", "call")
)


class classproperty[T](property):
    """
    Decorator for a Class-level property.
    No, I don't really know how it works.
    Credit to hottwaj on GitHub: https://github.com/hottwaj/classproperties
    """

    @override
    def __init__(self, func: Callable[[Any], T]) -> None:
        super().__init__(func)

    @override
    def __get__(self, owner_self: object, owner_cls: type | None = None) -> T:
        if owner_cls is None:
            raise TypeError

        if self.fget:
            return cast(T, self.fget(owner_cls))

        raise TypeError(f"{owner_cls.__name__} has no property {self.fget.__name__}")


def py_consts_to_types(*possible_consts: Any) -> tuple[type["CObject"], ...]:
    return (COpNotImplemented,) * len(possible_consts)


def _ctype_meta_factory() -> type[type]:
    """
    Helper function to create the CObjectMeta class.
    :returns: A metaclass for :py:class:`CObject` s named CObjectMeta
    """

    def op_generator(operation: str, args: int, err: type[Exception] | None = None) -> Callable[..., type["CObject"]]:
        locs: dict[str, any] = locals()
        exec(
            f'def op_impl(cls: type["CObject"], {", ".join([f'a{i}: Any' for i in range(args)])}) -> type["CObject"]:\n'
            + (
                f"\tif all(type.__instancecheck__(type, t) for t in ({", ".join(f"a{i}" for i in range(args))})):\n"
                if args > 1
                else ("\tif type.__instancecheck__(type, a0):\n" if args == 1 else "")
            )
            + ("\t" if args > 0 else "")
            + f'\tif "__{operation}__" in type.__dir__(cls) or (type.__instancecheck__(gens.CGenericMeta, cls) and "__{operation}__" in type.__dir__(type(cls))):\n'
            + ("\t" if args > 0 else "")
            + f'\t\tif callable(getattr(cls, "__{operation}__")):\n'
            + ("\t" if args > 0 else "")
            # + f'\t\t\tprint(getattr(cls, "__{operation}__"))\n'
            + f'\t\t\treturn cast(type["CObject"], getattr(cls, "__{operation}__")({", ".join([f"a{i}" for i in range(args)])}))\n'
            + ("\t" if args > 0 else "")
            # + f'\t\tprint(getattr(cls, "__{operation}__"))\n'
            + f'\t\treturn cast(type["CObject"], getattr(cls, "__{operation}__"))\n'
            + (
                (
                    "\telse:\n"
                    f'\t\tif "__{operation}_val__" in type.__dir__(cls) or (type.__instancecheck__(gens.CGenericMeta, cls) and "__{operation}_val__" in type.__dir__(type(cls))):\n'
                    # f'\t\t\tprint(f"{operation}: {{getattr(cls, "__{operation}_val__")}}")\n'
                    f'\t\t\treturn cast(type["CObject"], getattr(cls, "__{operation}_val__")({", ".join([f"a{i}" for i in range(args)])}))\n'
                    f'\t\tif "__{operation}__" in type.__dir__(cls) or (type.__instancecheck__(gens.CGenericMeta, cls) and "__{operation}__" in type.__dir__(type(cls))):\n'
                    # f'\t\t\tprint(f"{operation}: {{getattr(cls, "__{operation}__")}}")\n'
                    f'\t\t\treturn cast(type["CObject"], getattr(cls, "__{operation}__")(*py_consts_to_types({", ".join([f"a{i}" for i in range(args)])})))\n'
                )
                if args > 0
                else ""
            )
            + (f"\traise {err.__name__}" if err else "\treturn COpNotImplemented"),
            globals(),
            locs,
        )
        return locs["op_impl"]

    def any_op(operation: str) -> Callable[[type["CObject"]], type["CObject"]]:
        def op_impl(cls: type["CObject"], *args: type["CObject"], **kwargs: type["CObject"]) -> type["CObject"]:
            if operation in type.__dir__(cls):
                return cast(type["CObject"], getattr(cls, operation)(*args, **kwargs))
            return COpNotImplemented

        return op_impl

    methods: dict[str, Callable[..., Any]] = {
        "__hash__": type.__hash__,
        "__getattr__": op_generator("getattr", 1, AttributeError),
    }

    for op_name in _maths_operators + _comparisons + _container_operators + ("subclasscheck",):
        methods[f"__{op_name}__"] = op_generator(op_name, 1)

    for op_name in _unary_maths_operators + ("str", "bool"):
        methods[f"__{op_name}__"] = op_generator(op_name, 0)

    for op_name in _mutable_container_operators:
        methods[f"__{op_name}__"] = op_generator(op_name, 2)

    for op_name in ("call",):
        methods[f"__{op_name}__"] = any_op(op_name)

    created_metaclass: type[type] = type("CObjectMeta", (type,), methods)
    return created_metaclass


CObjectMeta: type[type] = _ctype_meta_factory()


class CObject(metaclass=CObjectMeta):  # type: ignore[misc] # mypy complains about our metaclass
    """
    Base class for all C++ types.
    """

    @classproperty
    def c(cls) -> str | None:
        """
        Name of the class in C++.
        ``None`` if the class cannot be converted to a C++ type.
        """
        return None

    @classproperty
    def headers(cls) -> Iterable[str]:
        """
        Tuple of C++ headers to import on usage.
        """
        return tuple()

    @classmethod
    def __str__(cls) -> str:  # type: ignore[override]
        """
        String representation of the C++ type, as specified in :py:attr:`c`.
        """
        if cls.c:
            return cls.c
        raise CTypeNotConcreteError(f"{cls.__name__} cannot be converted to a C++ type.")

    @classmethod
    def __bool__(cls) -> bool:
        """
        Whether this class is concrete.
        """
        return True

    @classmethod
    def __eq__(cls, other: type[Self]) -> type["CObject"]:  # type: ignore[override]
        """
        What type this class returns when compared with equality.
        """
        import pyg3a.types.numbers

        return pyg3a.types.numbers.CBool

    @classmethod
    def __ne__(cls, other: type[Self]) -> type["CObject"]:  # type: ignore[override]
        """
        What type this class returns when compared with inequality.
        """
        import pyg3a.types.numbers

        return pyg3a.types.numbers.CBool

    @classmethod
    def __subclasscheck__(cls, subclass: type) -> bool:
        """
        Whether ``subclass`` is a subclass of this class.
        """
        if type.__subclasscheck__(cls, subclass):
            return True

        return False


class COpNotImplemented(CObject):
    """
    Blank C++ type returned when an operation is not implemented on a class.
    """

    c = None

    @classmethod
    @override
    def __bool__(cls) -> bool:
        """
        Returns that this class is not concrete.
        """
        return False


def _cany_op_impl(_cls: type["CAny"], *_args: type["CObject"]) -> type["CAny"]:
    """
    Helper function to implement :py:class:`CAny`.
    All operations are acceptable and all return :py:class:`CAny`.
    """
    return CAny


CAny: type[CObject] = CObjectMeta(
    "CAny",
    (CObject,),
    {"c": "auto", "__subclasscheck__": classmethod(lambda cls, subclass: isinstance(subclass, CObjectMeta))}
    | {
        f"__{op}__": classmethod(_cany_op_impl)
        for op in _maths_operators
        + _comparisons
        + _container_operators
        + _mutable_container_operators
        + _unary_maths_operators
    },
)

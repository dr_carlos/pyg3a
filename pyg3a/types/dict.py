from typing import override

from .mapping import CMutableMapping
from .generics import GenericArg


class CDict(CMutableMapping):
    """
    Dicts are a mutable mapping with two generic arg which are the key required when accessing an element and value returned from access.
    """

    headers: tuple[str] = ("dict.hpp",)
    "Requires ``dict.hpp`` to instantiate."

    @classmethod
    @override
    def name(cls, bases: tuple[GenericArg, ...]) -> str:
        """
        Determines the name of a dict instance.

        :param bases: A list of bases consisting of 2 :py:class:`pyg3a.types.object.CObject` s.
        :returns: ``Dict<K, V>`` where K is the C++ equivalent of the first base type and V is the equivalent of the second.
        """
        return f"Dict<{bases[0]}, {bases[1]}>"

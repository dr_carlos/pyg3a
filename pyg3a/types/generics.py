import abc
import functools
from types import EllipsisType
from typing import Iterable, Final, Self, Sequence, final, TypeIs, Any, Callable

from .object import CObjectMeta, CObject, classproperty, all_operators

type GenericArg = EllipsisType | type[CObject] | list[GenericArg]


@functools.cache
def _impl_generator(klass: type["CGenericMeta"]) -> type["CGenericMeta"]:
    """
    Function to generate subclasses of generic metaclasses to actually instantiate generics with.
    This allows overriding all operators to use the correct CObjectMeta version.
    Cached so that type(CList[CInt]) is type(CList[CInt])

    :param klass: Generic metaclass to generate impl for
    :returns: New generic metaclass extending ``klass``
    """

    def generic_op_generator(op: str) -> Callable[..., Any]:
        def generic_op_impl(self: CGenericMeta, *all_args: Any) -> Any:
            # Use the CObjectMeta versions of everything (which call the appropriate operator or _val operator)
            # instead of the overriden class versions
            return getattr(super(CGenericMeta, self), f"__{op}__")(*all_args)

        return generic_op_impl

    return type(klass)(
        f"{klass.__name__}Impl",
        (klass,),
        {
            "__hash__": type.__hash__,
            **{f"__{op}__": generic_op_generator(op) for op in all_operators},
        },
    )


class CGenericMeta(CObjectMeta, abc.ABCMeta):  # type: ignore[misc,valid-type] # mypy complains about our metaclass
    """
    Abstract metaclass for Python generic/C++ template types.
    Can take any number of arguments, which may be ellipses, C++ types, or a list of arguments.
    """

    origin: type["CGenericMeta"]
    "The original generic type."

    args: tuple[GenericArg, ...]
    "The arguments given to create this generic."

    @classmethod
    def _generate_class_name(cls, bases: tuple[GenericArg, ...]) -> str:
        """
        Helper method to generate the name of a generic class created by this metaclass.

        :param bases: The arguments given to create this generic.
        :return: The 'hashed' name of the class.
        """
        return f"{cls.__name__}_{"_".join([b.__name__ if isinstance(b, type) else str(b) for b in bases])}"

    @classmethod
    @abc.abstractmethod
    def name(cls, bases: tuple[GenericArg, ...]) -> str | None:
        """
        Function to generate the name of a generic created by this metaclass from the given bases.

        :param bases: The arguments given to create this generic.
        :returns: The name of the class, or ``None`` if the class is not concrete.
        """
        ...

    @classmethod
    @abc.abstractmethod
    def acceptable_args(cls, bases: tuple[GenericArg, ...]) -> bool:
        """
        Determine whether the given bases are acceptable arguments for this generic.

        :param bases: The arguments given to create this generic.
        :returns: ``True`` if the arguments are acceptable, ``False`` otherwise.
        """
        ...

    @classproperty
    def headers(cls) -> Iterable[str]:
        """
        Property to get the C++ headers to import on usage.
        """
        return tuple()

    @classproperty
    def _base_classes(cls) -> Iterable[type[CObject]]:
        """
        Property for the base classes of instances of this generic.
        """
        return (CObject,)

    @classmethod
    def check_subclass_args(cls, args: Sequence[GenericArg], subclass_args: Sequence[GenericArg]) -> bool:
        """
        Check if the given subclass args are a subclass of the given args.
        By default, this is a simple check for subclassing on CObjects, identity on ellipses, and recursion on lists.

        :param args: The args to check.
        :param subclass_args: The subclass args to check against.
        :returns: ``True`` if the subclass args are a subclass of the args, ``False`` otherwise.
        """
        if len(args) != len(subclass_args):
            return False

        for arg, subclass_arg in zip(args, subclass_args):
            if arg is subclass_arg:
                continue

            if isinstance(arg, CObjectMeta) and isinstance(subclass_arg, CObjectMeta) and issubclass(subclass_arg, arg):
                continue

            if isinstance(arg, list) and isinstance(subclass_arg, list) and cls.check_subclass_args(arg, subclass_arg):
                continue

            return False

        return True

    @final
    def __subclasscheck__(self, subclass: type) -> TypeIs[Self]:
        """
        Determine whether ``subclass`` is a subclass of this type.

        :param subclass: The type to check against.
        :returns: ``True`` if this generic is a subclass of ``subclass``, ``False`` otherwise.
        """
        # A[B] is subclass of C if C inherits A[B]
        if type.__subclasscheck__(self, subclass):
            return True

        # A[B] is not a subclass of C if C is not a generic
        if not isinstance(subclass, CGenericMeta):
            return False

        # A[B] is not a subclass of C[X] if C does not inherit A
        if not issubclass(subclass.origin, self.origin):
            return False

        # A[B] is a subclass of C[X] if X == B
        if self.args == subclass.args:
            return True

        # A[B, C] is a subclass of D[X, Y] if B is a subclass of X and C is a subclass of Y
        if type(self).check_subclass_args(self.args, subclass.args):
            return True

        return False

    @classmethod
    def __class_getitem__(cls, *args: GenericArg) -> "CGenericMeta":
        """
        Create an instance of this generic with the given arguments.

        :param args: The arguments to use to create this generic.
        :returns: An instance of this generic with the given arguments.
        """
        type_args: tuple[GenericArg, ...] = args[0] if isinstance(args[0], tuple) else args

        if not cls.acceptable_args(type_args):
            return GenericNotImplemented

        def generic_instance_op_generator(op: str) -> Callable[..., Any]:
            def generic_instance_op_impl(self: CGenericMeta, *all_args: Any) -> Any:
                return getattr(cls, f"__{op}__")(self, *all_args)

            return generic_instance_op_impl

        return _impl_generator(cls)(
            cls._generate_class_name(type_args),
            tuple(cls._base_classes),
            {
                "c": classproperty(lambda c: c.name(type_args)),
                "headers": tuple(cls.headers),
                "origin": cls,
                "args": type_args,
                **{
                    f"__{op}__": classmethod(generic_instance_op_generator(op))
                    for op in all_operators
                    if f"__{op}__" in type.__dir__(cls) and getattr(cls, f"__{op}__").__name__ == f"__{op}__"
                },
            },
        )


class GenericNotImplementedMeta(CGenericMeta):
    """
    Placeholder class for when a generic is not concrete/cannot be implemented in C++.
    """

    @classmethod
    @abc.abstractmethod
    def name(cls, bases: tuple[GenericArg, ...]) -> str | None:
        """
        Function to generate the name of a generic created by this metaclass from the given bases.

        :param bases: The arguments given to create this generic.
        :returns: The name of the class, or ``None`` if the class is not concrete.
        """
        return "GenericNotImplemented"

    @classmethod
    @abc.abstractmethod
    def acceptable_args(cls, bases: tuple[GenericArg, ...]) -> bool:
        """
        Determine whether the given bases are acceptable arguments for this generic.

        :param bases: The arguments given to create this generic.
        :returns: ``True`` if the arguments are acceptable, ``False`` otherwise.
        """
        return True

    @classmethod
    def __bool__(cls) -> bool:
        """
        Returns ``False`` as this generic is not concrete.
        """
        return False

    @classmethod
    def __str__(cls) -> str:
        return "GenericNotImplemented"


GenericNotImplemented: Final[GenericNotImplementedMeta] = GenericNotImplementedMeta[...]

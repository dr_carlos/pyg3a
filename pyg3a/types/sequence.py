from typing import override

from .generics import CGenericMeta, GenericArg
from .misc import CNoneType
from .numbers import CInt, CBool
from .object import CObject, COpNotImplemented, CObjectMeta


class CSequence(CGenericMeta):
    """
    Abstract class inherited by all sequence types.
    """

    args: tuple[type[CObject]]
    "Accepts one type object as an argument."

    @classmethod
    @override
    def acceptable_args(cls, bases: tuple[GenericArg, ...]) -> bool:
        """
        Accepts one type object as an argument.

        :param bases: The arguments given to create this generic.
        :returns: ``True`` if the arguments are acceptable, ``False`` otherwise.
        """

        return len(bases) == 1 and isinstance(bases[0], CObjectMeta)

    def sequence_op(self: type[CObject], other: type[CObject]) -> type[CObject]:
        """
        Helper function for binary operations with another sequence.
        The least inherited type is returned.

        :param other: Other type in binary operation.
        :returns: The resultant type of this operation, or :py:class:`~pyg3a.types.object.COpNotImplemented` if the operation cannot be performed.
        """

        if not isinstance(other, CSequence):
            return COpNotImplemented

        if issubclass(other, self):
            return self

        if issubclass(self, other):
            return other

        return COpNotImplemented

    def sequence_access(self, other: type[CObject]) -> type[CObject]:
        """
        Helper function for operations that include access to items of this sequence.

        :param other: Type of index used to access a sequence element.
        :returns: The resultant type of this operation, or :py:class:`~pyg3a.types.object.COpNotImplemented` if the operation cannot be performed.
        """

        if issubclass(other, CInt):
            return self.args[0]

        return COpNotImplemented

    def __add__(self, other: type[CObject]) -> type[CObject]:
        return self.sequence_op(other)

    def __getitem__(self, other: type[CObject]) -> type[CObject]:
        return self.sequence_access(other)

    def __contains__(self, other: type[CObject]) -> type[CObject]:
        if self.sequence_access(other):
            return CBool
        return COpNotImplemented


class CMutableSequence(CSequence):
    """
    Abstract class inherited by all mutable sequence types.
    Inherits from :py:class:`CSequence` and implements :py:meth:`__setitem__` and :py:meth:`__delitem__` .
    """

    def __setitem__(self, key: type[CObject], value: type[CObject]) -> type[CObject]:
        if issubclass(value, self.sequence_access(key)):
            return CNoneType
        return COpNotImplemented

    def __delitem__(self, other: type[CObject]) -> type[CObject]:
        if self.sequence_access(other):
            return CNoneType
        return COpNotImplemented

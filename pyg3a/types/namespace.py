# Must import first
from .generics import CGenericMeta
from .numbers import CInt, CBool, CFloat, CPointer
from .object import CObject, COpNotImplemented, CObjectMeta, CAny

from .misc import CNoneType, CEllipsisType, CExplicit
from .sequence import CSequence
from .callable import CCallable
from .string import CStr
from .list import CList
from .dict import CDict
from .enum import CEnum, CEnumType
from .tuples import CTuple, CSpecificTuple, CArbitraryLengthTuple
from .type_alias import CTypeVar, CGenericTypeAlias

"""
Namespace storing references to type objects from their Python name.
Implemented using a NamedTuple for memory efficiency.
"""

__all__ = [
    "object",
    "int",
    "bool",
    "float",
    "str",
    "list",
    "tuple",
    "dict",
    "type",
    "NotImplemented",
    "Any",
    "Callable",
    "Sequence",
    "Generic",
    "TypeVar",
    "TypeAliasType",
    "NoneType",
    "EllipsisType",
    "Enum",
    "EnumType",
    "SpecificTuple",
    "ArbitraryLengthTuple",
    "Explicit",
    "Pointer",
]

# Built-ins
object = CObject
int = CInt
bool = CBool
float = CFloat
str = CStr
list = CList
tuple = CTuple
dict = CDict
NotImplemented = COpNotImplemented

# typing stdlib
Any = CAny  # type: ignore
Callable = CCallable
Sequence = CSequence
Generic = CGenericMeta
TypeVar = CTypeVar
TypeAliasType = CGenericTypeAlias

# types stdlib
NoneType = CNoneType
EllipsisType = CEllipsisType

# enum stdlib
Enum = CEnum
EnumType = CEnumType

# Our custom types
SpecificTuple = CSpecificTuple
ArbitraryLengthTuple = CArbitraryLengthTuple
Explicit = CExplicit
Pointer = CPointer

# Inbuilt type - has to be last
type = CObjectMeta

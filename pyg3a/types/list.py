from typing import override

from .generics import GenericArg
from .sequence import CMutableSequence


class CList(CMutableSequence):
    """
    Lists are a mutable sequence with one generic arg which is the value returned when accessing an element.
    """

    headers: tuple[str] = ("list.hpp",)
    "Requires ``list.hpp`` to instantiate."

    @classmethod
    @override
    def name(cls, bases: tuple[GenericArg, ...]) -> str:
        """
        Determines the name of a list instance.

        :param bases: A list of bases consisting of 1 :py:class:`pyg3a.types.object.CObject` .
        :returns: ``List<T>`` where T is the C++ equivalent of the base type.
        """
        return f"List<{bases[0]}>"

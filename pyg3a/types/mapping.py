from typing import override

from .generics import CGenericMeta, GenericArg
from .misc import CNoneType
from .numbers import CBool
from .object import CObject, CObjectMeta, COpNotImplemented


class CMapping(CGenericMeta):
    """
    Abstract class inherited by all sequence types.
    """

    args: tuple[type[CObject], type[CObject]]
    "Accepts key, value type objects as arguments."

    @classmethod
    @override
    def acceptable_args(cls, bases: tuple[GenericArg, ...]) -> bool:
        """
        Accepts key, value type objects as arguments.

        :param bases: The arguments given to create this generic.
        :returns: ``True`` if the arguments are acceptable, ``False`` otherwise.
        """

        return len(bases) == 2 and isinstance(bases[0], CObjectMeta) and isinstance(bases[1], CObjectMeta)

    def mapping_access(self, other: type[CObject]) -> type[CObject]:
        """
        Helper function for operations that include access to items of this mapping.

        :param other: Type of key used to access a sequence element.
        :returns: The value type of this mapping, or :py:class:`~pyg3a.types.object.COpNotImplemented` if the operation cannot be performed.
        """

        if issubclass(other, self.args[0]):
            return self.args[1]

        return COpNotImplemented

    def __getitem__(self, other: type[CObject]) -> type[CObject]:
        return self.mapping_access(other)

    def __contains__(self, other: type[CObject]) -> type[CObject]:
        return CBool if self.mapping_access(other) else COpNotImplemented


class CMutableMapping(CMapping):
    """
    Abstract class inherited by all mutable mapping types.
    Inherits from :py:class:`CMapping` and implements :py:meth:`__setitem__` and :py:meth:`__delitem__` .
    """

    def __setitem__(self, key: type[CObject], value: type[CObject]) -> type[CObject]:
        return CNoneType if issubclass(value, self.mapping_access(key)) else COpNotImplemented

    def __delitem__(self, other: type[CObject]) -> type[CObject]:
        return CNoneType if self.mapping_access(other) else COpNotImplemented

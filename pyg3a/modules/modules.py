#!/usr/bin/env python3
import builtins
import dataclasses
from pathlib import Path
from typing import Final, Any, Self

import libcst as cst

import pyg3a
from .functions import Function, importer, FunctionCall, FunctionFor
from ..node import node_to_code
from ..scope import NoValue, Scope
from ..types import Types


@dataclasses.dataclass(slots=True, init=False)
class Module:
    """
    Class for representing and interpreting importable modules.
    """

    name: Final[str]
    "This module's name."

    functions: Final[dict[Function, None]]
    "Key dict of :py:class:`Function`s contained in this module in insertion order."

    main_func: Function | None
    "The main function of this module (run on import), if specified."

    scope: Scope
    "Scope in which this module is imported into."

    def __init__(self, name: str, scope: Scope | None = None) -> None:
        """
        Set up Module, generating its set of functions and immediately including required C headers and adding custom types.

        :param name: The name of this module.
        :param scope: The scope in which this module is imported into.
        """

        self.name = name
        self.functions = {}
        self.main_func = None
        self.scope = scope.inner() if scope else pyg3a.Main.globs.inner()

    def parse_file(self, file_path: Path) -> Self:
        """
        Parse a Python file defining this module, as in :py:meth:`parse_text`.

        :param file_path: Path to the Python file defining this module.
        :returns: The parsed module.
        """
        # Open the module file and parse its contents
        with file_path.open() as f:
            self.parse_text(f.read())

        return self

    def parse_text(self, module_contents: str) -> Self:
        """
        Parse the contents of a Python file defining this module and:
        - Generate this module's :py:attr`functions` set from ``func`` and ``func__iter__`` functions.
        - Register custom types specified in ``__registry_types_pyg3a`` functions.
        - Import C/C++ headers from ``import header`` and ``from header import`` statements. \
        See :py:meth:`~pyg3a.pyg3a.Project.include_from_python_name` for header name rules.

        :param module_contents: A string representing the contents of a Python file defining this module.
        :returns: the parsed module.
        """

        # For statement in module
        for stmt in cst.parse_module(module_contents).body:
            # Parse functions in module
            match stmt:
                case cst.ClassDef(name=py_name):
                    custom_builtins: dict[str, Any] = builtins.__dict__.copy()
                    custom_builtins["__import__"] = importer
                    custom_builtins["eval"] = lambda code: eval(code, {"__builtins__": {}}, {})
                    custom_builtins["exec"] = lambda code: eval(code, {"__builtins__": {}}, {})

                    new_type_globs: dict[str, Any] = {
                        "Types": Types,
                        "__builtins__": custom_builtins,
                    }
                    for name, var in self.scope:
                        if (issubclass(var.type, Types.type) and var.value is not NoValue) or (
                            issubclass(var.type, type) and issubclass(var.value, Types.type)
                        ):
                            new_type_globs[name] = var.value

                    exec(node_to_code(stmt), new_type_globs)

                    if isinstance(new_type_globs[py_name.value], Types.type) or (
                        isinstance(new_type_globs[py_name.value], type)
                        and issubclass(new_type_globs[py_name.value], Types.Generic)
                    ):
                        self.scope.set_var(py_name, type(new_type_globs[py_name.value]), new_type_globs[py_name.value])

                case cst.FunctionDef(name=cst.Name(value="__main__")):
                    self.main_func = FunctionCall(stmt, self)

                # Add custom functions and __for_pyg3a_* functions from module to custom_functions dictionary
                case cst.FunctionDef(name=cst.Name(value=name)) if name[-8:] == "__iter__":
                    self.functions[FunctionFor(stmt, self)] = None
                case cst.FunctionDef():
                    self.functions[FunctionCall(stmt, self)] = None

                # Parse imports in module or constant definitions
                case cst.SimpleStatementLine(body=statements):
                    for simple_stmt in statements:
                        match simple_stmt:
                            case cst.Import(names=headers):
                                # For header in imports
                                for alias in headers:
                                    pyg3a.Main.project.include_from_python_name(node_to_code(alias.name))

                            # Include headers from ``from header import _``
                            case cst.ImportFrom(module=(cst.Module() | cst.Attribute() as header)):
                                pyg3a.Main.project.include_from_python_name(node_to_code(header))

                            # Constant type definitions
                            case cst.Assign(
                                targets=[cst.AssignTarget(target=cst.Name(value="DEFINES"))],
                                value=(cst.Dict() as definitions),
                            ) | cst.AnnAssign(target=cst.Name(value="DEFINES"), value=(cst.Dict() as definitions)):
                                new_type_globs: dict[str, Any] = {"Types": Types, "__builtins__": {}}
                                for name, var in self.scope:
                                    if issubclass(var.type, Types.type) and var.value is not NoValue:
                                        new_type_globs[name] = var.value

                                for name, typ in eval(node_to_code(definitions), new_type_globs).items():
                                    self.scope.set_type(
                                        cst.Name(value=name),
                                        Types.Explicit(typ) if isinstance(typ, str) else typ,
                                    )

                            # Unhandled statements
                            case _:
                                raise SyntaxError(
                                    f"Unsupported statement '{node_to_code(simple_stmt)}' of type '{type(simple_stmt)}' - Expected a function, import, or class definition"
                                )

                # Unhandled statements
                case _:
                    raise SyntaxError(
                        f"Unsupported statement '{node_to_code(stmt)}' of type '{type(stmt)}' - Expected a function, import, or class definition"
                    )

        return self

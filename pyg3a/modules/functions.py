#!/usr/bin/env python3
import abc
import builtins
import functools
import inspect
import textwrap
import types
from abc import ABC
from dataclasses import dataclass, field
from types import FunctionType, GenericAlias, ModuleType
from typing import (
    Any,
    Final,
    NamedTuple,
    Optional,
    TypeVar,
    cast,
    Callable,
    MutableMapping,
    Sequence,
    Mapping,
    Self,
    Generic,
)

import libcst as cst

import pyg3a
from ..block import Block
from ..errors import NotAnnotatedError, TranspileError
from ..functions import Function as CFunction, Parameter as CSTParameter, CTemplateParam
from ..logging import logger
from ..node import node_to_code, node_type, node_to_c_str
from ..py_consts import node_to_py_const, Constant, CSTConstant
from ..scope import Scope, NoValue
from ..type_utils import cst_annotation_to_type, py_annotation_to_type
from ..types import Types, GenericArg

FuncTypes = cst.Call | cst.For


class Argument[T]:
    """
    Argument to a module function.
    """

    node: cst.BaseExpression
    "The argument's CST node."

    typ: Types.type
    "The argument's C++ type."

    t_val: type[T]
    "The argument's Python type."

    @classmethod
    def from_obj(cls, from_: T, node: cst.BaseExpression, typ: Types.type = Types.object) -> Self:
        new_cls: type[Self] = type(cls)(
            f"{type(from_).__name__}Argument", (cls, from_.t_val if isinstance(from_, Argument) else type(from_)), {}
        )
        arg: Self = new_cls(from_)
        arg.node = node
        arg.typ = typ
        arg.t_val = type(from_)
        return arg


class ArgumentInst[T](Argument[T]):
    def __add__(self, other):
        if isinstance(other, Argument):
            if issubclass(self.t_val, str):
                return ArgumentInst.from_obj(
                    super(Generic, self).__add__(f" + {other}"),
                    cst.BinaryOperation(self.node, cst.Add(), other.node),
                    self.typ + other.typ,
                )

            return ArgumentInst.from_obj(
                super(Generic, self).__add__(other),
                cst.BinaryOperation(self.node, cst.Add(), other.node),
                self.typ + other.typ,
            )

        else:
            if issubclass(self.t_val, str):
                return ArgumentInst.from_obj(
                    super(Generic, self).__add__(f" + {other}"),
                    cst.BinaryOperation(self.node, cst.Add(), cst.parse_expression(str(other).strip())),
                    self.typ + py_annotation_to_type(type(other), pyg3a.Main.globs),
                )

            return ArgumentInst.from_obj(
                super(Generic, self).__add__(other),
                cst.BinaryOperation(self.node, cst.Add(), cst.parse_expression(str(other).strip())),
                self.typ + py_annotation_to_type(type(other), pyg3a.Main.globs),
            )

    def __iadd__(self, other):
        return self.__add__(other)

    def __radd__(self, other):
        if isinstance(other, Argument):
            if issubclass(self.t_val, str):
                return ArgumentInst.from_obj(
                    super(Generic, other).__add__(f" + {self}"),
                    cst.BinaryOperation(other.node, cst.Add(), self.node),
                    other.typ + self.typ,
                )

            return ArgumentInst.from_obj(
                super(Generic, other).__add__(self),
                cst.BinaryOperation(other.node, cst.Add(), self.node),
                other.typ + self.typ,
            )
        else:
            if issubclass(self.t_val, str):
                return ArgumentInst.from_obj(
                    other.__add__(f" + {self}"),
                    cst.BinaryOperation(cst.parse_expression(str(other).strip()), cst.Add(), self.node),
                    self.typ,
                )

            return ArgumentInst.from_obj(
                other.__add__(self),
                cst.BinaryOperation(cst.parse_expression(str(other).strip()), cst.Add(), self.node),
                self.typ,
            )

    def __str__(self) -> "ArgumentInst[str]":
        return ArgumentInst.from_obj(super().__str__(), self.node, Types.str)

    def __int__(self) -> "ArgumentInst[int]":
        if issubclass(self.t_val, int):
            return ArgumentInst.from_obj(self, self.node, Types.int)
        elif issubclass(self.t_val, str):
            return ArgumentInst.from_obj(int(super().__str__()), self.node, Types.int)

        raise NotImplementedError(f"Sorry, not implemented yet on {self.t_val.__name__}")

    # def __index__(self) -> "ArgumentInst[int]":
    #     return ArgumentInst.from_obj(operator.index(self.t_val(self)), self.node, Types.int)
    #
    # def __float__(self) -> "ArgumentInst[float]":
    #     return ArgumentInst.from_obj(float(self.t_val(self)), self.node, Types.float)


class Parameter(NamedTuple):
    """
    Tuple representing a parsed parameter.
    """

    name: str
    "The name of the Parameter."
    type: Types.type
    "Its Python type."
    default: Constant | NoValue
    "Its default value (must be a Python constant), or Never if no default is specified."
    to_arg: Callable[[cst.Arg, Scope], ArgumentInst]
    "The function used to convert an argument passed into this parameter into a C string or \
    other object to be used inside module functions."


def importer(
    name: str,
    globals: Optional[Mapping[str, object]] = None,
    locals: Optional[Mapping[str, object]] = None,
    fromlist: Optional[Sequence[str]] = tuple(),
    level: int = 0,
) -> ModuleType | None:
    """
    Custom __import__ function used when executing module functions.
    Includes the specified header into the output C++.

    :param name: The module or header name to import (see :py:meth:`~pyg3a.Project.include_from_python_name`).
    :param globals: Globals dictionary from the import statement's scope.
    :param locals: Locals dictionary from the import statement's scope.
    :param fromlist: The list of functions to import from a module.
    :param level: Whether to perform relative or absolute imports.
    :raises ImportError: If a local variable has already been defined which including this header would override.
    """

    # If importing from "." (i.e. no module name), we're importing a custom function from another package
    if not name and fromlist:

        def module_function(func_name: str, *args: Argument) -> Argument[str]:
            func_call: cst.Call = cst.Call(
                func=cst.Name(value=func_name),
                args=tuple(
                    [
                        (
                            cst.Arg(value=a.node)
                            if isinstance(a, Argument)
                            else cst.Arg(value=cst.parse_expression(repr(a)))
                        )
                        for a in args
                    ]
                ),
            )

            scope: Scope = globals.get("__pyg3a_scope", pyg3a.Main.globs)
            return ArgumentInst.from_obj(
                scope.convert_from(
                    func_call,
                ),
                func_call,
                scope.get_conversion_return_type_for(func_call),
            )

        module: ModuleType = ModuleType("this_module")
        for func in fromlist:
            setattr(module, func, functools.partial(module_function, func))

        return module

    # Check if we're trying to include a header that would override a local variable
    if locals and name.split(".", maxsplit=1)[0] in locals:
        raise ImportError("Cannot import module with same name as local variable", name=name)

    # Otherwise, include the header from the module name
    pyg3a.Main.project.include_from_python_name(name)


def _c_func_decorator(parent_mod_name: str, func_prefix: str, func: FunctionType) -> Callable[..., Argument[str]]:
    """
    Decorator used when executing module functions.

    Denotes that the decorated ``func`` represents a raw C function.

    Under the hood it adds a C function whose signature is translated from ``func``'s,
    and whose body is the output of running ``func`` with ``None`` passed as all arguments.
    This function is added to :py:attr:`~pyg3a.pyg3a.Main.globs`.

    :param parent_mod_name: The name of the module the decorated function is defined in.
    :param func_prefix: A prefix to add to the function name to avoid collisions. Usually includes name of parent function and type vars.
    :param func: The decorated function.
    :raises NotAnnotatedError: If there is no return type specified for ``func``.
    """

    if "return" not in func.__annotations__:
        raise NotAnnotatedError(f"No return annotation on function {func.__name__}() from module '{parent_mod_name}'")

    scope: Scope = func.__globals__.get("__pyg3a_scope", pyg3a.Main.globs)
    ret: Types.type = py_annotation_to_type(func.__annotations__["return"], scope)

    func_c_body: str = textwrap.indent(textwrap.dedent(func(*((None,) * func.__code__.co_argcount))), "\t").strip()
    func_name: str = f"__{parent_mod_name}{func_prefix}_{func.__name__}"

    starargs_param: str | None
    kwargs_param: str | None

    (_, starargs_param, kwargs_param, _, _, _, _) = inspect.getfullargspec(func)

    pyg3a.PyG3A.add_c_func(
        func_name,
        (
            c_func := CFunction(
                cst.FunctionDef(
                    name=cst.Name(value=func_name),
                    params=cst.Parameters(),
                    body=cst.IndentedBlock([]),
                ),
                scope,
                name=func_name,
                ret=ret,
                node=None,
                statements=[cst.parse_statement(f'raw_c("""{func_c_body}""")')],
                params=[
                    CSTParameter(
                        name=cst.Name(value=param.name),
                        annotation=cst.Annotation(
                            annotation=(
                                cst.parse_expression(str(name[0]))
                                if (
                                    name := [
                                        name  # value
                                        for name, (typ, value) in scope
                                        if issubclass(typ, Types.type)
                                        and issubclass(py_annotation_to_type(param.annotation, scope), value)
                                        and issubclass(value, py_annotation_to_type(param.annotation, scope))
                                    ]
                                )
                                else cst.SimpleString(value=f"'{py_annotation_to_type(param.annotation, scope)}'")
                            ),
                            # annotation=cst.SimpleString(value=node_to_code(cst.parse_expression(param.annotation)))
                        ),
                        star=(
                            "*"
                            if param.kind == param.VAR_POSITIONAL
                            else "**" if param.kind == param.VAR_KEYWORD else ""
                        ),
                        default=(
                            cst.parse_expression(str(param.default)) if param.default is not param.empty else None
                        ),
                    )
                    for param in inspect.signature(func).parameters.values()
                ],
                template_params=(
                    [CTemplateParam(str(t)) for t in func.__type_params__]
                    + ([CTemplateParam("Args", variadic=True)] if starargs_param or kwargs_param else [])
                ),
            )
        ).construct(),
        [py_annotation_to_type(ann, scope) for name, ann in func.__annotations__.items() if name != "return"],
        ret,
    )

    return lambda *args: ArgumentInst.from_obj(
        f"{func_name}({', '.join([str(a) for a in args])})",
        cst.Call(
            func=cst.Name(func_name),
            args=tuple(
                [
                    cst.Arg(value=a.node) if isinstance(a, Argument) else cst.Arg(value=cst.parse_expression(repr(a)))
                    for a in args
                ]
            ),
        ),
        ret,
    )


def _struct_c_func_decorator(parent_mod_name: str, func_prefix: str, func: FunctionType) -> Callable[..., str]:
    """
    Decorator used when executing module functions.

    Denotes that the decorated ``func`` represents a raw C function that returns
    a struct which we should treat as a tuple.

    Under the hood it creates a struct named __struct_``function name`` with the same fields as
    the struct returned by ``func``. Then it adds a C function whose signature is translated from ``func``'s,
    and whose body is the output of running ``func`` with ``None`` passed as all arguments.
    This function is added to :py:attr:`~pyg3a.pyg3a.Main.globs`, and the struct is added
    to the :py:attr:`~pyg3a.pyg3a.Main.type_registry`.

    :param parent_mod_name: The name of the module the decorated function is defined in.
    :param func_prefix: A prefix to add to the function name to avoid collisions. Usually includes name of parent function and type vars.
    :param func: The decorated function.
    :raises NotAnnotatedError: If there is no return type specified for ``func``.
    :raises TypeError: If the return type is not a tuple.
    """

    if "return" not in func.__annotations__:
        raise NotAnnotatedError(f"No return annotation on function {func.__name__}() from module '{parent_mod_name}'")

    if not (
        isinstance(func.__annotations__["return"], GenericAlias) and func.__annotations__["return"].__origin__ is tuple
    ):
        raise TypeError("@struct_c_func can only be used on functions that return a tuple")

    scope: Scope = func.__globals__.get("__pyg3a_scope", pyg3a.Main.globs)

    func_c_body: str = textwrap.indent(textwrap.dedent(func(*((None,) * func.__code__.co_argcount))), "\t").strip()
    struct_name: str = f"__struct_{parent_mod_name}{func_prefix}{func.__name__}"

    struct_type: Types.SpecificTuple = Types.SpecificTuple.struct(struct_name)[
        tuple(py_annotation_to_type(t, scope) for t in func.__annotations__["return"].__args__)
    ]

    # Add struct to C project
    pyg3a.PyG3A.add_c_func(
        struct_name,
        f"struct {struct_name} {{\n"
        + "\n".join(
            [
                f"\t{py_annotation_to_type(t, scope)} _{i};"
                for i, t in enumerate(func.__annotations__["return"].__args__)
            ]
        )
        + "\n};",
        [py_annotation_to_type(t, scope) for t in func.__annotations__["return"].__args__],
        struct_type,
    )

    # Add struct type to global scope now that C is set
    pyg3a.Main.globs.set_var(cst.Name(struct_name), Types.SpecificTuple, struct_type)

    # Adjust the return annotation for later
    func.__annotations__["return"] = struct_name

    def init(self: object) -> None:
        self.__repr__ = lambda: struct_name

    struct_annotation: type = type(struct_name, (), {"__slots__": "__repr__", "__init__": init})

    func_name: str = f"__{parent_mod_name}{func_prefix}_{func.__name__}"

    pyg3a.PyG3A.add_c_func(
        func_name,
        CFunction(
            cast(
                cst.FunctionDef,
                cst.parse_statement(
                    # Map struct_name: str(struct_name) so we evaluate it to itself
                    f'def {func_name}{("[" + ", ".join([str(t) for t in func.__type_params__]) + "]") if func.__type_params__ else ""}{inspect.signature(
                        func,
                        globals={struct_name: struct_annotation()},
                        eval_str=True
                    )}:\n\traw_c("""{func_c_body}""")'
                ),
            ),
            scope,
        ).construct(),
        [py_annotation_to_type(ann, scope) for name, ann in func.__annotations__.items() if name != "return"],
        py_annotation_to_type(func.__annotations__["return"], scope),
    )

    return lambda *args: f"{func_name}({', '.join([str(a) for a in args])})"


def _syscall_decorator(parent_mod_name: str, number: int) -> Callable[[FunctionType], None]:
    """
    Decorator used when executing module functions.

    This adds a new function which runs syscall ``number`` to :py:attr:`~pyg3a.pyg3a.Main.globs` whose \\
    name and signature is that of the decorated function.

    :param parent_mod_name: The name of the module the decorated function is defined in.
    :param number: The syscall's number (usually specified in hexadecimal).
    :raises NotAnnotatedError: If there is no return type specified for ``func``.
    """

    def wrapper(func: FunctionType) -> None:
        if "return" not in func.__annotations__:
            raise NotAnnotatedError(
                f"No return annotation on function {func.__name__}() from module '{parent_mod_name}'"
            )

        scope: Scope = func.__globals__.get("__pyg3a_scope", pyg3a.Main.globs)

        ret_annotation: Types.type = py_annotation_to_type(func.__annotations__["return"], scope)
        param_annotations: dict[str, Types.type] = {
            p_name: py_annotation_to_type(p_ann, scope)
            for p_name, p_ann in func.__annotations__.items()
            if p_name != "return"
        }

        pyg3a.PyG3A.add_c_func(
            f"__pyg3a_asm_{func.__name__}",
            f'extern "C" {ret_annotation} {func.__name__}({
            ', '.join(
                [
                    f"{ann} {name}" for name, ann in param_annotations.items()
                ]
            )
            });\n__asm__(".text; .align 2; .global _{func.__name__}; _{func.__name__}: mov.l sc_addr, r2; mov.l 1f, r0; jmp @r2; nop; 1: .long {number}; sc_addr: .long 0x80020070");',
            list(param_annotations.values()),
            ret_annotation,
        )

    return wrapper


@dataclass(slots=True, init=False)
class Function(ABC):
    """
    High-level generic representation of a module function for a specific Node type.

    Currently supporting :py:class:`libcst.Call` and :py:class:`libcst.For`.
    """

    name: Final[str]
    "The name of this function."

    parent_mod_name: Final[str]
    "The name of the module this function is defined in."

    func_def: Final[cst.FunctionDef]
    "The underlying :py:class:`libcst.FunctionDef` for this function."

    params: Final[tuple[Parameter, ...]]
    "Standard parameters for this function."

    posonly_params: Final[tuple[Parameter, ...]]
    "Positional-only parameters for this function."

    kwonly_params: Final[tuple[Parameter, ...]]
    "Keyword-only parameters for this function."

    starargs: Final[Optional[Parameter]]
    "Optional parameter collecting all other positional arguments for this function."

    kwargs: Final[Optional[Parameter]]
    "Optional parameter collecting all other keyword arguments for this function."

    type_params: Final[tuple[str, ...]]
    "The type parameters for this function."

    def __init__(self, func_def: cst.FunctionDef, parent_module: "pyg3a.modules.Module", name: str) -> None:
        """
        Initialise the function from a CST definition, generating and classifying its keyword parameters.
        Positional and standard parameters are handled in subclass inits.

        :param func_def: The CST definition of this function.
        :param parent_module: The module this function is defined in.
        :param name: The friendly name of this function.
        :raises TypeError: If the function is a :py:class:`libcst.For` and has no positional params.
        :raises NotAnnotatedError: If there is no return type annotated in the ``func_def``.
        """
        if func_def.returns is None:
            raise TranspileError(NotAnnotatedError("Return type must be annotated"), func_def)

        self.name = name
        self.parent_mod_name = parent_module.name
        self.func_def = func_def

        if func_def.type_parameters:
            self.type_params = tuple([param.param.name.value for param in func_def.type_parameters.params])
        else:
            self.type_params = tuple()

        self._generate_positional_params(parent_module)

        self.kwonly_params = tuple(
            [self._gen_param(param, parent_module.scope) for param in func_def.params.kwonly_params]
        )

        self.kwargs = (
            self._gen_param(func_def.params.star_kwarg, parent_module.scope) if func_def.params.star_kwarg else None
        )
        self.starargs = (
            self._gen_param(func_def.params.star_arg, parent_module.scope)
            if isinstance(func_def.params.star_arg, cst.Param)
            else None
        )

    @abc.abstractmethod
    def _generate_positional_params(self, parent_module: "pyg3a.modules.Module"): ...

    def _gen_param(self, p: cst.Param, scope: Scope) -> Parameter:
        """
        Generate a :py:class:`Parameter` from a :py:class:`libcst.Param` node.

        :param p: The CST Param node.
        :returns: A generated :py:class:`Parameter` tuple.
        :raises NotAnnotatedError: If the parameter is missing an annotation.
        :raises TypeError: If a non-constant default is set.
        """

        if p.annotation is None:
            raise TranspileError(NotAnnotatedError("Parameters must have annotations."), p)

        if p.default is not None and not isinstance(p.default, CSTConstant.__value__):
            raise TranspileError(TypeError("Default parameters must be constants."), p)

        if isinstance(p.annotation.annotation, cst.List):
            return Parameter(
                p.name.value,
                Types.tuple[
                    *[
                        cst_annotation_to_type(el.value, scope, type_params=self.type_params)
                        for el in p.annotation.annotation.elements
                    ]
                ],
                NoValue if p.default is None else node_to_py_const(cast(CSTConstant, p.default)),
                lambda arg, scope: ArgumentInst.from_obj(
                    [node_to_c_str(elem.value, scope) for elem in arg.value.elements],
                    arg.value,
                    node_type(arg.value, scope),
                ),
            )

        return Parameter(
            p.name.value,
            cst_annotation_to_type(p.annotation.annotation, scope, type_params=self.type_params),
            NoValue if p.default is None else node_to_py_const(cast(CSTConstant, p.default)),
            lambda arg, scope: ArgumentInst.from_obj(
                node_to_c_str(arg.value, scope), arg.value, node_type(arg.value, scope)
            ),
        )

    @property
    @abc.abstractmethod
    def accepted_node(self) -> type[FuncTypes]: ...

    @abc.abstractmethod
    def _get_functioninstance_cls(self) -> type["FunctionInstance"]: ...

    def accepts(self, node: FuncTypes, scope: Scope) -> Optional["FunctionInstance"]:
        """
        Check if this function accepts the given node.

        :param node: The node to check.
        :param scope: The enclosing scope the node is referenced in.
        :returns: A :py:class:`FunctionInstance` generated from ``node`` and ``scope``
        if the function accepts the node, else None.
        """

        inst: FunctionInstance = self._get_functioninstance_cls()(
            self, node, scope, cst_annotation_to_type(self.func_def.returns.annotation)
        )
        if inst.acceptable():
            return inst

        return None


@dataclass(slots=True, init=False, unsafe_hash=True)
class FunctionCall(Function):
    def __init__(self, func_def: cst.FunctionDef, parent_module: "pyg3a.modules.Module") -> None:
        super(FunctionCall, self).__init__(func_def, parent_module, func_def.name.value)

    @property
    def accepted_node(self) -> type[FuncTypes]:
        return cst.Call

    def _get_functioninstance_cls(self) -> type["FunctionInstance"]:
        return FunctionInstanceCall

    def _generate_positional_params(self, parent_module: "pyg3a.modules.Module"):
        self.posonly_params = tuple(
            [self._gen_param(param, parent_module.scope) for param in self.func_def.params.posonly_params]
        )
        self.params = tuple([self._gen_param(param, parent_module.scope) for param in self.func_def.params.params])


@dataclass(slots=True, init=False, unsafe_hash=True)
class FunctionFor(Function):
    multiple_variables: Final[bool]
    "If a cst.For node and multiple variables may be iterated over."

    def __init__(self, func_def: cst.FunctionDef, parent_module: "pyg3a.modules.Module") -> None:
        super(FunctionFor, self).__init__(func_def, parent_module, func_def.name.value.split("__iter__")[0])

    @property
    def accepted_node(self) -> type[FuncTypes]:
        return cst.For

    def _get_functioninstance_cls(self) -> type["FunctionInstance"]:
        return FunctionInstanceFor

    def _generate_positional_params(self, parent_module: "pyg3a.modules.Module"):
        # If the __iter__ function has pos-only params, then the first one is the var name
        # (i.e. it's not passed in by calls)
        if self.func_def.params.posonly_params:
            self.posonly_params = tuple(
                [self._gen_param(param, parent_module.scope) for param in self.func_def.params.posonly_params[1:]]
            )
            self.params = tuple([self._gen_param(param, parent_module.scope) for param in self.func_def.params.params])

            self.multiple_variables = not issubclass(
                cst_annotation_to_type(
                    self.func_def.params.posonly_params[0].annotation.annotation, parent_module.scope
                ),
                Types.str,
            )

        # Else if the __iter__ function has standard params, then the first one is the var name
        elif self.func_def.params.params:
            self.posonly_params = tuple(
                [self._gen_param(param, parent_module.scope) for param in self.func_def.params.posonly_params]
            )
            self.params = tuple(
                [self._gen_param(param, parent_module.scope) for param in self.func_def.params.params[1:]]
            )

            self.multiple_variables = not issubclass(
                cst_annotation_to_type(self.func_def.params.params[0].annotation.annotation, parent_module.scope),
                Types.str,
            )

        # We don't support passing the var name as a keyword param, so error
        else:
            raise TranspileError(
                TypeError("__iter__ functions must have a positional parameter to pass the var name into"),
                self.func_def,
            )


# noinspection PyFinal
@dataclass(slots=True)
class FunctionInstance:
    """
    Instance of a :py:class:`Function` for a specific node and scope.
    """

    function: Final[Function]
    "The :py:class:`Function` this is an instance of."

    node: Final[FuncTypes]
    "The node this function is called with."

    scope: Final[Scope]
    "The enclosing scope the node is referenced in."

    return_type: Types.type
    "The return type of this function instance (including type variable substitution)."

    complete_args: list[cst.Arg | Parameter] = field(default_factory=list)
    "A complete list of passed arguments and parameters to this function. Generated in :py:meth:`acceptable()`."

    complete_params: tuple[Parameter] = field(default_factory=tuple)
    "A complete list of parameters to this function (taking into account pos/any/kw args and stars). Generated in :py:meth:`acceptable()`."

    type_vars: MutableMapping[str, Types.type] = field(default_factory=dict)
    "A map of type variables to their actual types. Generated in :py:meth:`acceptable()`."

    @abc.abstractmethod
    def get_call(self) -> cst.Call: ...

    def acceptable(self) -> bool:
        """
        Determine if this instance's node and scope are acceptable for this instance's function.

        Also generates :py:attr:`complete_args`.

        :returns: True if the node is acceptable, else False.
        :raises AssertionError: If the node is a :py:class:`libcst.For` and it's not iterating over a function call.
        """

        call: cst.Call = self.get_call()

        # If too many args, we don't accept
        if (
            not self.function.kwargs
            and not self.function.starargs
            and len(call.args)
            > (len(self.function.params) + len(self.function.kwonly_params) + len(self.function.posonly_params))
        ):
            logger.debug("2")
            return False

        # If not enough args for just standard params, short-circuit and don't accept
        if len(call.args) < len([p for p in self.function.params if p.default is NoValue]):
            logger.debug("3")
            return False

        # Get the passed positional args
        pos_args: list[cst.Arg] | None = []
        kw_args: list[cst.Arg] | None = []

        for arg in call.args:
            if arg.keyword:
                # We're not worrying about evaluating **kwargs arguments
                if arg.star:
                    kw_args = None
                elif kw_args is not None:
                    kw_args.append(arg)
            else:
                # We're not worrying about evaluating *args arguments
                if arg.star:
                    pos_args = None
                elif pos_args is not None:
                    pos_args.append(arg)

        # If we can't process the pos and kw arg counts, short-circuit to accept
        if kw_args is None and pos_args is None:
            logger.debug("4")
            return True

        # Expanded list of arguments or their parameters if defaulted
        self.complete_args = []

        # The number of positional args
        checked_posargs: int = 0

        # If pos_args is None, we won't process the posargs
        if pos_args is not None:
            # Now we'll check the positional arguments
            self.complete_args.extend(pos_args)

            # Fill in with posonly defaults
            if len(pos_args) < len(self.function.posonly_params):
                for param in self.function.posonly_params[checked_posargs:]:

                    # If we haven't specified an argument, and it doesn't have a default param, don't accept
                    if param.default is NoValue:
                        logger.debug("5")
                        return False

                    # Otherwise add the default's type to the list of specified args
                    self.complete_args.append(param)
                    checked_posargs += 1

            checked_posargs = len(pos_args) - len(self.function.posonly_params)

            # If we have too many positional arguments to fill the positional-or-keyword params, don't accept
            # Unless we have starargs, in which case we can have as many positional args as we want
            if checked_posargs > len(self.function.params) and not self.function.starargs:
                logger.debug("6")
                return False

        checked_kwargs: int = 0

        # If kw_args is None, we won't process the kwargs
        if kw_args is not None:
            # Map argument names
            arg_map: dict[str, cst.Arg] = {arg.keyword.value: arg for arg in kw_args}

            # Now generate the complete_args from our map
            for param in self.function.params[checked_posargs:] + self.function.kwonly_params:
                # Use default if necessary
                if param.name not in arg_map:
                    # If it's not specified and there's no default, don't accept
                    if param.default is NoValue:
                        logger.debug("7")
                        return False

                    # Otherwise add the default's type to the list of specified args
                    self.complete_args.append(param)
                else:
                    self.complete_args.append(arg_map[param.name])

                checked_kwargs += 1

        # Map of type_var -> type
        self.type_vars = {}

        self.complete_params = (
            (self.function.posonly_params if pos_args is not None else tuple())
            + self.function.params
            + (self.function.starargs,)
            * (checked_posargs - len(self.function.posonly_params) - len(self.function.params))
            + (self.function.kwonly_params if kw_args is not None else tuple())
            + (self.function.kwargs,)
            * (checked_kwargs - len(self.function.kwonly_params) - abs(len(self.function.params) - checked_posargs))
        )

        # This should hopefully work, but really I should write a test for it
        try:
            for arg, param in zip(
                self.complete_args,
                self.complete_params,
                strict=True,
            ):
                if isinstance(arg, Parameter):
                    # We must have defaulted so let's assume for now that it's fine
                    continue

                # The parameter's type - i.e. the expected type for the argument
                expected: Types.type = param.type

                # The argument's type
                actual: Types.type | type[Types.type] = node_type(arg.value, self.scope)

                # If the argument is not concrete, it's not acceptable
                if not isinstance(actual, Types.type):
                    logger.debug("8")
                    return False

                # If we're expecting a TypeVar, set the type var and continue to next argument
                if issubclass(expected, Types.TypeVar):
                    self.type_vars[expected.var_name] = actual
                    continue

                # If we're expecting a generic instance (e.g. a 'list[str]' instance)
                if isinstance(expected, Types.Generic):

                    # But the argument isn't a generic instance, it's not acceptable
                    if not isinstance(actual, Types.Generic):
                        logger.debug("9")
                        return False

                    # If the origin classes don't match, it's not acceptable
                    if not issubclass(actual.origin, expected.origin):
                        logger.debug("10")
                        return False

                    # Expected arguments from the specified parameter type
                    expected_args: tuple[GenericArg, ...] = expected.args

                    # For each generic arg to the specified parameter
                    for i, generic_arg in enumerate(expected.args):

                        # If the generic arg is a TypeVar and the actual arg is concrete, save the type var and update expected_args
                        if (
                            isinstance(generic_arg, Types.type)
                            and issubclass(generic_arg, Types.TypeVar)
                            and isinstance(actual.args[i], Types.type)
                        ):
                            expected_args = expected_args[:i] + (actual.args[i],) + expected_args[i + 1 :]
                            self.type_vars[generic_arg.var_name] = actual.args[i]

                    # Update our expected type from the generated args
                    expected = expected.origin[*expected_args]

                # If the argument's type doesn't match the expected parameter type, it's not acceptable
                if not issubclass(actual, expected):
                    logger.debug("11")
                    return False

                pass
        except ValueError:
            logger.debug("12")
            return False

        return True

    def _call_get_args(self) -> tuple[list[object], dict[str, object]]:
        """
        Get this instance's positional and keyword arguments to pass to its function.

        This requires having run :py:meth:`__init__` first to generate :py:attr:`complete_args`,
        and :py:meth:`acceptable` to generate :py:attr:`complete_params`.

        :returns: A tuple of ([positional arguments], {keyword name: keyword arguments}).
        """

        posonly_args: list[ArgumentInst]
        kwonly_args: dict[str, ArgumentInst]

        return [
            # Convert the argument to a string or other type, according to the parameter's to_arg()
            param.to_arg(arg, self.scope)
            for arg, param in zip(
                # All passed arguments/default parameters
                self.complete_args,
                # All function params
                self.complete_params,
            )
            # Only convert non-keyword arguments. Default params are all at the end so don't bother.
            if isinstance(arg, cst.Arg) and not arg.keyword
        ], {
            # Convert the argument to a string or other type, according to the parameter's to_arg()
            arg.keyword.value: param.to_arg(arg, self.scope)
            for arg, param in zip(
                # All passed arguments/default parameters
                self.complete_args,
                # All function params
                self.complete_params,
            )
            # Only convert keyword arguments. Default params are all at the end so don't bother.
            if isinstance(arg, cst.Arg) and arg.keyword
        }

    def eval_module_func(self, *args: str, **kwargs: str) -> str:
        """
        Evaluate this instance's function with specified positional and keyword arguments.

        Under the hood, this reconstructs the instance's function definition without any annotations, then defines
        new builtins such as ``__import__`` to include and ``eval`` to evaluate safely, defines decorators,
        generates the new function definition code, and calls the function with a mangled name.

        :param args: Positional arguments to pass to the function.
        :param kwargs: Keyword arguments to pass to the function.
        :returns: The C equivalent of the function call.
        :raises RuntimeError: If the called function doesn't return a non-empty string.
        """

        func_def: cst.FunctionDef = self.function.func_def

        untyped_funcdef: cst.FunctionDef = cst.FunctionDef(
            name=cst.Name(f"__f{self.function.name}"),
            body=(
                cst.IndentedBlock(
                    body=[cst.SimpleStatementLine(body=[cst.Expr(cst.Call(func=cst.Name("locals")))])]
                    + list(func_def.body.body),
                    indent=func_def.body.indent,
                )
                if isinstance(func_def.body, cst.IndentedBlock)
                else cst.SimpleStatementSuite(
                    body=[cst.Expr(cst.Call(func=cst.Name("locals")))]
                    + list(cast(cst.SimpleStatementSuite, func_def.body).body)
                )
            ),
            params=cst.Parameters(
                params=[cst.Param(param.name, default=param.default) for param in func_def.params.params],
                star_arg=(
                    cst.Param(func_def.params.star_arg.name, default=func_def.params.star_arg.default)
                    if type(func_def.params.star_arg) is cst.Param
                    else func_def.params.star_arg
                ),
                kwonly_params=[cst.Param(param.name, default=param.default) for param in func_def.params.kwonly_params],
                star_kwarg=(
                    cst.Param(func_def.params.star_kwarg.name, default=func_def.params.star_kwarg.default)
                    if func_def.params.star_kwarg
                    else None
                ),
                posonly_params=[
                    cst.Param(param.name, default=param.default) for param in func_def.params.posonly_params
                ],
                posonly_ind=func_def.params.posonly_ind,
            ),
        )

        custom_builtins: dict[str, Any] = dict(builtins.__dict__)
        custom_builtins["__import__"] = importer
        custom_builtins["eval"] = lambda code: eval(code, {"__builtins__": {}}, {})
        custom_builtins["exec"] = lambda code: eval(code, {"__builtins__": {}}, {})

        globs: dict[str, Any] = {
            "__builtins__": custom_builtins,
            "c_func": functools.partial(
                _c_func_decorator,
                self.function.parent_mod_name,
                self.function.name + "".join([str(t) for t in self.type_vars.values()]),
            ),
            "struct_c_func": functools.partial(
                _struct_c_func_decorator,
                self.function.parent_mod_name,
                self.function.name + "".join([str(t) for t in self.type_vars.values()]),
            ),
            "syscall": functools.partial(_syscall_decorator, self.function.parent_mod_name),
            "logger": logger,
            "Types": Types,
            "__pyg3a_scope": self.scope,
        }

        def init(sel: object, name: str) -> None:
            sel.__repr__ = lambda: name

        for custom_type, (typ, val) in self.scope:
            if custom_type not in builtins.__dict__:
                if issubclass(typ, Types.type):
                    globs[custom_type] = type(
                        custom_type,
                        (),
                        {"__slots__": "__repr__", "__init__": functools.partial(init, name=custom_type)},
                    )
                elif issubclass(typ, type) and issubclass(val, Types.Generic):
                    globs[custom_type] = type(
                        custom_type,
                        (),
                        {
                            "__class_getitem__": lambda cls, key: types.GenericAlias(cls, key),
                            "__init__": functools.partial(init, name=custom_type),
                        },
                    )

        for var_name, var_type in self.type_vars.items():
            globs[var_name] = var_type

        locs = globs.copy()
        exec(node_to_code(untyped_funcdef), globs, locs)

        evaluated: Any = locs[f"__f{self.function.name}"](*args, **kwargs)

        # If a global variable has been declared
        if globs.keys() - locs.keys():
            for var_name in globs.keys() - locs.keys():
                # If a warning is raised, __warningregistry__ will be set to a dict, and we don't want to handle this
                if var_name == "__warningregistry__":
                    continue

                # Declare variable if it doesn't exist
                if var_name not in pyg3a.Main.globs:
                    pyg3a.Main.project.add_var(var_name, globs[var_name])

                pyg3a.Main.globs.set_var(
                    cst.Name(var_name), py_annotation_to_type(type(globs[var_name]), self.scope), globs[var_name]
                )

        if evaluated and isinstance(evaluated, str):
            return cast(str, evaluated)

        raise TranspileError(
            RuntimeError(
                f"Function '{self.function.name}' from module '{self.function.parent_mod_name}' did not return a string."
            ),
            self.node,
        )

    def update_return(self) -> None:
        self.return_type = cst_annotation_to_type(
            self.function.func_def.returns.annotation, self.scope, [str(t) for t in self.type_vars]
        )

        if issubclass(self.return_type, Types.TypeVar):
            self.return_type = self.type_vars[self.return_type.var_name]

        if isinstance(self.return_type, Types.Generic):
            generic_args: tuple[GenericArg, ...] = self.return_type.args

            for i, generic_arg in enumerate(self.return_type.args):
                if isinstance(generic_arg, Types.type) and issubclass(generic_arg, Types.TypeVar):
                    generic_args = generic_args[:i] + (self.type_vars[generic_arg.var_name],) + generic_args[i + 1 :]

            self.return_type = self.return_type.origin[*generic_args]

    @abc.abstractmethod
    def convert(self) -> str:
        """
        Convert this function instance into a C string.

        :returns: The C equivalent of the node.
        """

        self.update_return()

        return ""


@dataclass(slots=True)
class FunctionInstanceCall(FunctionInstance):
    function: Final[FunctionCall]

    def get_call(self) -> cst.Call:
        return self.node

    def convert(self) -> str:
        """
        Converter implementation for :py:class:`libcst.Call` nodes.

        :returns: The C equivalent of the function call.
        """

        super(FunctionInstanceCall, self).convert()

        args: list[object]
        kwargs: dict[str, object]

        args, kwargs = self._call_get_args()
        return self.eval_module_func(*args, **kwargs)


@dataclass(slots=True)
class FunctionInstanceFor(FunctionInstance):
    function: Final[FunctionFor]

    def get_call(self) -> cst.Call:
        # All For nodes are iterating over a callable
        return self.node.iter

    def convert(self, tab_count: int = 0) -> str:
        super(FunctionInstanceFor, self).convert()

        """
        Converter implementation for :py:class:`libcst.For` nodes.

        :param tab_count: The number of tabs to use for indentation.
        :returns: The C equivalent of the for loop.
        """

        if self.function.multiple_variables:
            assert isinstance(self.node.target, cst.Tuple)
            assert all(isinstance(e.value, cst.Name) for e in self.node.target.elements)
        else:
            assert isinstance(self.node.target, cst.Name)

        args: list[object]
        kwargs: dict[str, object]

        args, kwargs = self._call_get_args()
        args.insert(
            0,
            (
                tuple([e.value.value for e in self.node.target.elements])
                if self.function.multiple_variables
                else self.node.target.value
            ),
        )
        lines: list[str] = [f"{tab_count * "\t"}{self.eval_module_func(*args, **kwargs)}"]

        try:
            if self.function.multiple_variables:
                assert isinstance(self.return_type, Types.tuple)

            expressions = Block(
                self.node.body.body,
                tab_count + 1,
                self.scope.inner(
                    (
                        tuple([e.value.value for e in self.node.target.elements])
                        if self.function.multiple_variables
                        else self.node.target.value
                    ),
                    (self.return_type.args if self.function.multiple_variables else self.return_type),
                ),
            )
            lines.append(expressions.construct())
        except AssertionError as err:
            raise TranspileError(err, self.function.func_def.returns)

        lines.append(tab_count * "\t" + "}")

        return "\n".join(lines)

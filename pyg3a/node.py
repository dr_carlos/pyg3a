#!/usr/bin/env python3
import collections
import functools
import warnings
from operator import and_
from typing import Final

import libcst as cst

import pyg3a
from .errors import CTypeNotConcreteError, FStringTooComplexError, TranspileError, TranspileWarning
from .logging import logger
from .py_consts import py_const_to_c_str, node_to_py_const, CSTConstant, Constant
from .scope import Scope, NoValue
from .type_utils import cst_annotation_to_type
from .types import Types

CST_TO_C_EQV: Final[dict[type[cst.CSTNode], str]] = {
    # Statements
    cst.Break: "break",
    cst.Continue: "continue",
    cst.Pass: "/* PASS */",
    #
    # Comparison operators
    cst.Equal: "==",
    cst.Is: "==",
    cst.NotEqual: "!=",
    cst.IsNot: "!=",
    cst.GreaterThan: ">",
    cst.GreaterThanEqual: ">=",
    cst.LessThan: "<",
    cst.LessThanEqual: "<=",
    #
    # Logical operators
    cst.Or: "||",
    cst.And: "&&",
    cst.Not: "!",
    #
    # Numerical operators
    cst.Add: "+",
    cst.Subtract: "-",
    cst.Multiply: "*",
    cst.Divide: "/",
    cst.Modulo: "%",
    cst.Plus: "+",
    cst.Minus: "-",
    # cst.Power - covered in cst.BinaryOperation as it requires math.h.
    # cst.FloorDivide - covered in cst.BinaryOperation as it requires math.h.
    #
    # Binary operators
    cst.BitAnd: "&",
    cst.BitOr: "|",
    cst.BitXor: "^",
    cst.BitInvert: "~",
    cst.LeftShift: "<<",
    cst.RightShift: ">>",
    #
    # Operative assignment
    cst.AddAssign: "+=",
    cst.SubtractAssign: "-=",
    cst.MultiplyAssign: "*=",
    cst.DivideAssign: "/=",
    cst.ModuloAssign: "%=",
    cst.BitAndAssign: "&=",
    cst.BitOrAssign: "|=",
    cst.BitXorAssign: "^=",
    cst.LeftShiftAssign: "<<=",
    cst.RightShiftAssign: ">>=",
    # cst.FloorDivideAssign - covered in cst.AugAssign as it does not have a 1-1 in C
}
"Dictionary mapping CST node types to their C string equivalent."


def node_type(
    node: cst.CSTNode,
    scope: Scope,
) -> Types.type | type[Types.type]:
    """
    Determine type of CST node under the specified scope.

    :param node: The CST node to determine the type of.
    :param scope: The scope to find variables' types inside.
    :returns: The type of the specified ``node``. If the type cannot be determined, the ``Any`` type is returned.
    :raises RuntimeError: If a variable is referenced with no type specified in ``scope``.
    """

    match node:
        # Ellipsis
        case cst.Ellipsis():
            return Types.EllipsisType

        # Numbers
        case cst.Imaginary():
            return Types.NotImplemented
        case cst.Integer():
            return Types.int
        case cst.Float():
            return Types.float

        # Strings
        case cst.BaseString():
            return Types.str

        # True, False, None
        case cst.Name(value="True"):
            return Types.bool
        case cst.Name(value="False"):
            return Types.bool
        case cst.Name(value="None"):
            return Types.NoneType

        # Variables
        case cst.Name(value=var_name):
            if var_name in scope:
                return scope[var_name].type
            raise TranspileError(RuntimeError(f"Variable {var_name} not found in scope"), node)

        # Enums
        case cst.Attribute(value=cst.Name(value=enum), attr=cst.Name()) if enum in scope and issubclass(
            scope[enum].type, Types.EnumType
        ):
            return Types.int

        # Called functions
        case cst.Call() as call if (ret_type := scope.get_conversion_return_type_for(call)):
            return ret_type
        case cst.Call(func=cst.Name(value=func_name)) if func_name in scope and isinstance(
            scope[func_name].type, Types.Callable
        ):
            return scope[func_name].type.args[1]
        case cst.Call(func=cst.Name(value="cast"), args=[cst.Arg(value=cst.Name(value=typ)), _]):
            return scope[typ].value

        # Iterable[Index] or Generic[Args]
        case cst.Subscript(value=iterable, slice=[cst.SubscriptElement(slice=cst.Index(value=index))]):
            if isinstance(iterable_type := node_type(iterable, scope), Types.type) or issubclass(
                iterable_type, Types.Generic
            ):
                try:
                    return iterable_type[node_to_py_const(index)]
                except TranspileError:
                    return iterable_type[node_type(index, scope)]
            return Types.NotImplemented

        # Lambdas
        case cst.Lambda(params, body):
            return_type: Types.type | type[Types.type] = node_type(body, scope)
            if isinstance(return_type, Types.type):
                return Types.Callable[[Types.Any for _ in params.posonly_params + params.params], return_type]
            raise TranspileError(CTypeNotConcreteError(return_type), body)

        # Sequences
        case cst.Tuple(elements=elems):
            elem_types: tuple[type[Types.type | Types.object], ...] = tuple(
                [node_type(elem.value, scope) for elem in elems]
            )
            if all([isinstance(elem, Types.type) for elem in elem_types]):
                return Types.tuple[*elem_types]

            raise TranspileError(CTypeNotConcreteError(elem_types), elems)
        case cst.List(elements=elems):
            if elems:
                return Types.list[node_type(elems[0].value, scope)]
            return Types.list[Types.Any]
        case cst.Dict(elements=elems):
            if elems:
                return Types.dict[node_type(elems[0].key, scope), node_type(elems[0].value, scope)]
            return Types.dict[Types.Any, Types.Any]
        # Operators
        case cst.BinaryOperation(left=left, operator=operator, right=right):
            left_type: Types.type | type[Types.type] = node_type(left, scope)
            right_type: Types.type | type[Types.type] = node_type(right, scope)

            if issubclass(left_type, Types.type) or issubclass(right_type, Types.type):
                return Types.NotImplemented

            match operator:
                case cst.Add():
                    return left_type + right_type
                case cst.Subtract():
                    return left_type - right_type
                case cst.Multiply():
                    return left_type * right_type
                case cst.Divide():
                    return left_type / right_type
                case cst.Modulo():
                    return left_type % right_type
                case cst.Power():
                    return left_type**right_type
                case cst.FloorDivide():
                    return left_type // right_type
                case cst.LeftShift():
                    return left_type << right_type
                case cst.RightShift():
                    return left_type >> right_type
                case cst.BitAnd():
                    return left_type & right_type
                case cst.BitOr():
                    return left_type | right_type
                case cst.BitXor():
                    return left_type ^ right_type
                case _:
                    return Types.NotImplemented

        case cst.Comparison(left=left, comparisons=comparisons):
            comparison_types: list[Types.type] = []
            left_type: Types.type | type[Types.type] = node_type(left, scope)

            if issubclass(left_type, Types.type):
                return Types.NotImplemented

            for comparison in comparisons:
                right_type: Types.type | type[Types.type] = node_type(comparison.comparator, scope)

                if issubclass(right_type, Types.type):
                    return Types.NotImplemented

                match comparison.operator:
                    case cst.Equal():
                        comparison_types.append(left_type == right_type)
                    case cst.NotEqual():
                        comparison_types.append(left_type != right_type)
                    case cst.LessThan():
                        comparison_types.append(left_type < right_type)
                    case cst.LessThanEqual():
                        comparison_types.append(left_type <= right_type)
                    case cst.GreaterThan():
                        comparison_types.append(left_type > right_type)
                    case cst.GreaterThanEqual():
                        comparison_types.append(left_type >= right_type)
                    case cst.In() | cst.NotIn() if isinstance(right_type, Types.Sequence):
                        comparison_types.append(Types.bool)
                    case cst.Is() | cst.IsNot():
                        comparison_types.append(Types.bool)
                    case _:
                        return Types.NotImplemented

            # Get most derived base of comparison return types
            return next(iter(functools.reduce(and_, (collections.Counter(cls.mro()) for cls in comparison_types))))

        case cst.UnaryOperation(operator=op, expression=expr):
            expr_type: Types.type | type[Types.type] = node_type(expr, scope)

            if issubclass(expr_type, Types.type):
                return Types.NotImplemented

            match op:
                case cst.Plus():
                    return +expr_type

                case cst.Minus():
                    return -expr_type

                case cst.BitInvert():
                    return ~expr_type

                case cst.Not():
                    return Types.bool

                case _:
                    return Types.NotImplemented

    # Auto type if unsure
    return Types.Any


def node_to_code(node: cst.CSTNode) -> str:
    """
    Convert CST Node object to Python code.

    :param node: CST Node
    :returns: String containing Python code equivalent
    """
    return pyg3a.Main.codegen_module.code_for_node(node)


def _f_string_content_to_str(
    content: cst.BaseFormattedStringContent, scope: Scope, complex_mode: bool = False
) -> tuple[str, ...]:
    # If simple string section of f-string, return text
    if isinstance(content, cst.FormattedStringText):
        escaped_str: str = content.value.replace("}}", "}").replace("{{", "{").replace("%", "%%")
        if complex_mode:
            return (py_const_to_c_str(escaped_str),)
        return (escaped_str,)

    # Otherwise, it must be an expression part of f-string
    assert isinstance(content, cst.FormattedStringExpression)

    # Built format string
    fmt: list[str] = []

    def fmt_part(s: str) -> None:
        fmt.append(py_const_to_c_str(s) if complex_mode else s)

    # Add var_name= for f-string 'debug'
    if content.equal:
        fmt_part(f"{node_to_code(content.expression)}=")

    # Start substitution
    fmt_part("%")

    # If format specifier is provided
    if content.format_spec:
        for f in content.format_spec:
            fmt_and_exprs: tuple[str, ...]
            try:
                fmt_and_exprs = _f_string_content_to_str(f, scope)
                if complex_mode:
                    fmt_and_exprs = py_const_to_c_str(fmt_and_exprs[0]), *fmt_and_exprs[1:]
            except FStringTooComplexError:
                if complex_mode:
                    fmt_and_exprs = _f_string_content_to_str(f, scope, True)
                else:
                    raise FStringTooComplexError

            if len(fmt_and_exprs) == 1:
                fmt.append(fmt_and_exprs[0])
            else:
                if complex_mode:
                    fmt.append(f"_sprintf({fmt_and_exprs[0]}, {', '.join(fmt_and_exprs[1:])})")
                else:
                    raise FStringTooComplexError

    # Type of this expression
    expr_type: Types.type = node_type(content.expression, scope)

    # C strings of arguments to pass to sprintf
    substitute_args: list[str] = []

    # Add substitution type char
    if issubclass(expr_type, Types.str):
        fmt_part("s")
        substitute_args.append(f"{node_to_c_str(content.expression, scope)}.c_str()")
    elif issubclass(expr_type, Types.int):
        fmt_part("d")
        substitute_args.append(node_to_c_str(content.expression, scope))
    elif issubclass(expr_type, Types.float):
        raise TranspileError(SyntaxError("Floats are not supported in f-strings"), content)
    else:
        logger.warning("No conversion specifier determined for type, automatically using %s")
        fmt_part("s")
        substitute_args.append(f"String({node_to_c_str(content.expression, scope)}).c_str()")

    # Return format string and arguments
    return (" + " if complex_mode else "").join(fmt), *substitute_args


def node_to_c_str(node: cst.CSTNode, scope, /, *, is_type=False) -> str:
    match node:
        # If we're calling a function
        case cst.Call():
            # If we have a custom function defined for this function
            try:
                return scope.convert_from(node)
            except KeyError:
                pass

            match node:
                # If we're casting a value
                case cst.Call(func=cst.Name(value="cast"), args=[cst.Arg(value=typ), cst.Arg(value=val)]):
                    return f"({cst_annotation_to_type(typ, scope)}) ({node_to_c_str(val, scope)})"
                case cst.Call(func=func, args=arguments):
                    # If no custom __pyg3a_, just run the function
                    return f"{node_to_c_str(func, scope)}({', '.join([node_to_c_str(arg.value, scope) for arg in arguments])})"

        case cst.Name(value=value) as const:
            # Constants
            if value in ("True", "False", "None"):
                return py_const_to_c_str(node_to_py_const(const))

            # Known variable (e.g. type)
            if (
                value in scope
                and scope[value].value is not NoValue
                and (
                    is_type is (isinstance(scope[value].type, Types.type) or issubclass(scope[value].type, Types.type))
                )
            ):
                return str(scope[value].value)

            # Variable
            return value

        case cst.FormattedString(parts=parts):
            try:
                fmt: str = ""
                args: list[str] = []

                for part in parts:
                    part_fmt_and_args = _f_string_content_to_str(part, scope)
                    fmt += part_fmt_and_args[0]
                    args.extend(part_fmt_and_args[1:])

                return f"{scope.convert_from(cst.Call(func=cst.Name(value="__pyg3a_sprintf")))}({py_const_to_c_str(fmt)}{', ' if args else ''}{', '.join(args)})"
            except FStringTooComplexError:
                c_fmt: list[str] = []
                c_args: list[str] = []

                for part in parts:
                    part_fmt_and_args = _f_string_content_to_str(part, scope, True)
                    c_fmt.append(part_fmt_and_args[0])
                    c_args.extend(part_fmt_and_args[1:])

                return f"{scope.convert_from(cst.Call(func=cst.Name(value="__pyg3a_sprintf")))}({' + '.join(c_fmt)}{', ' if c_args else ''}{', '.join(c_args)})"

        # Other constants
        case node if isinstance(node, CSTConstant.__value__):
            py_const: Constant = node_to_py_const(node)

            # Explicit string types
            if is_type and isinstance(py_const, str):
                return py_const

            # Other constants
            return py_const_to_c_str(py_const)

        # Sli[c:e:s]
        case cst.Subscript(slice=[cst.SubscriptElement(slice=cst.Slice())]):
            raise TranspileError(SyntaxError("There is no support for slices"), node)

        # GenericOrigin[args]
        case cst.Subscript(value=cst.Name(value=origin)) as generic if is_type and origin in scope and issubclass(
            scope[origin].value, Types.Generic
        ):
            return str(cst_annotation_to_type(generic, scope))

        # tuple[index]
        case cst.Subscript(
            value=collection, slice=[cst.SubscriptElement(cst.Index(value=cst.Integer(value=index)))]
        ) if collection in scope and isinstance(scope[collection].type, Types.tuple):
            return f"{node_to_c_str(collection, scope)}._{index}"

        # Array/list/dict item access
        # Translate our item access directly to C
        case cst.Subscript(value=collection, slice=[cst.SubscriptElement(cst.Index(value=index))]):
            return f"{node_to_c_str(collection, scope)}[{node_to_c_str(index, scope)}]"

        # Translate Enum.member from stored enums
        case cst.Attribute(value=cst.Name(value=enum), attr=cst.Name(value=member)) if enum in scope and issubclass(
            scope[enum].type, Types.EnumType
        ):
            if scope[enum].value is not NoValue:
                return getattr(scope[enum].value, member)
            return member

        # struct.prop translates directly to C
        case cst.Attribute(value=struct, attr=cst.Name(prop)):
            return f"{node_to_c_str(struct, scope)}.{prop}"

        # Use CST_TO_C_EQV to translate comparisons, boolean, and unary operators
        case cst.Comparison(left, comparisons):

            def _comparison_to_c_str(l: cst.BaseExpression, op: cst.BaseCompOp, r: cst.BaseExpression) -> str:
                if isinstance(op, cst.Is) or isinstance(op, cst.IsNot):
                    l_is_const: bool = False
                    r_is_const: bool = False

                    try:
                        node_to_py_const(l)
                        l_is_const = True
                    except TranspileError:
                        pass

                    try:
                        node_to_py_const(r)
                        r_is_const = True
                    except TranspileError:
                        pass

                    # If we're comparing constants, check equality as usual
                    if l_is_const or r_is_const:
                        return f"{node_to_c_str(l, scope)} {"==" if isinstance(op, cst.Is) else "!="} {node_to_c_str(r, scope)}"

                    # If they're vars, check address equality
                    if isinstance(l, cst.Name) and isinstance(r, cst.Name) and l in scope and r in scope:
                        return f"&{node_to_c_str(l, scope)} {"==" if isinstance(op, cst.Is) else "!="} &{node_to_c_str(r, scope)}"

                    warnings.warn(
                        TranspileWarning(
                            f"Identity check always returns {"False" if isinstance(op, cst.Is) else "True"}", node
                        )
                    )

                    return "false" if isinstance(op, cst.Is) else "true"

                if isinstance(op, cst.In):
                    return scope.convert_from(
                        cst.Call(func=cst.Name(value="__pyg3a_contains"), args=(cst.Arg(value=l), cst.Arg(value=r))),
                    )

                if isinstance(op, cst.NotIn):
                    return f"(!{scope.convert_from(
                        cst.Call(func=cst.Name(value="__pyg3a_contains"), args=(cst.Arg(value=l), cst.Arg(value=r))),
                    )})"

                return f"{node_to_c_str(l, scope)} {CST_TO_C_EQV[type(op)]} {node_to_c_str(r, scope)}"

            # && separated comparisons
            return CST_TO_C_EQV[cst.And].join(
                [f"({_comparison_to_c_str(left, comp.operator, comp.comparator)})" for comp in comparisons]
            )

        case cst.BooleanOperation(left, operator, right):
            return f"({node_to_c_str(left, scope)} {CST_TO_C_EQV[type(operator)]} {node_to_c_str(right, scope)})"

        case cst.UnaryOperation(operator, expression):
            return f"({CST_TO_C_EQV[type(operator)]} {node_to_c_str(expression, scope)})"

        case cst.BinaryOperation(left=left, operator=cst.Power(), right=right) if node_type(left, scope) ** node_type(
            right, scope
        ):
            # Use <math.h>'s pow function for power operators.
            pyg3a.Main.project.includes.add("math.h")
            return f"pow({node_to_c_str(left, scope)}, {node_to_c_str(right, scope)})"

        case cst.BinaryOperation(left=left, operator=cst.FloorDivide(), right=right) if node_type(
            left, scope
        ) // node_type(right, scope):
            # Use casts for C equivalent of floor division.
            # If left and right are integers => left / right == left // right
            if issubclass(node_type(left, scope), Types.int) and issubclass(node_type(right, scope), Types.int):
                return f"({node_to_c_str(left, scope)} / {node_to_c_str(right, scope)})"

            # Otherwise, round left / right and then convert it to a float
            return f"({node_type(left, scope) // node_type(right, scope)}) ((int) ({node_to_c_str(left, scope)} / {node_to_c_str(right, scope)}))"

        case cst.BinaryOperation(left, operator, right) as op:
            # Otherwise use CST_C_EQV for supported operations
            if node_type(op, scope):
                return f"({node_to_c_str(left, scope)} {CST_TO_C_EQV[type(operator)]} {node_to_c_str(right, scope)})"

            try:
                raise TranspileError(
                    SyntaxError(
                        f"Unsupported types {(
                            node_type(left, scope)
                        )} and {(
                            node_type(right, scope)
                        )} for operation {type(operator).__name__}"
                    ),
                    node,
                )
            except CTypeNotConcreteError:
                raise TranspileError(
                    SyntaxError(
                        f"Unsupported types {(
                            node_type(left, scope)
                        ).__name__} and {(
                            node_type(right, scope)
                        ).__name__} for operation {type(operator).__name__}"
                    ),
                    node,
                )

        # Walrus operator
        case cst.NamedExpr(target=cst.Name(value=var_name)) if var_name not in scope:
            # You can't declare a variable in the C equivalent ((int a = b) doesn't syntax correctly),
            # so it must be something already in scope
            raise TranspileError(SyntaxError(f"type of variable '{var_name}' must be defined in scope"), node)

        case cst.NamedExpr(target=cst.Name(target), value=value):
            # a := b => a = b in C
            return f"({target} = {node_to_c_str(value, scope)})"

        # Lambdas
        case cst.Lambda(params=cst.Parameters(params), body=body):
            # You can't annotate lambda args, so we'll have to automatically determine their type
            args: list[str] = [f"auto {param.name.value}" for param in params]

            stmt: str = node_to_c_str(
                # Create inner scope from the lambda args with 'any' types
                body,
                scope.inner(params, "any"),
            )

            # Return outcome of lambda by default
            return f"[]({', '.join(args)}){{return {stmt};}}"

        # Ternary if
        case cst.IfExp(test, body, orelse=or_else):
            return f"({node_to_c_str(test, scope)} ? {node_to_c_str(body, scope)} : {node_to_c_str(or_else, scope)})"

        case cst.Dict(elements=elements):
            return f"{node_type(node, scope)}({{{', '.join([node_to_c_str(elt.key, scope) for elt in elements])}}}, {{{', '.join([node_to_c_str(elt.value, scope) for elt in elements])}}})"

        # Lists are initialiser lists with an explicit cast ( List<int>({0,1}) ) so they can be used as literals
        case cst.List(elements):
            return f"{node_type(node, scope)}{{{', '.join([node_to_c_str(elt.value, scope) for elt in elements])}}}"

        # Sets and tuples are struct initialisers
        case cst.Set(elements) | cst.Tuple(elements):
            return f"{{{', '.join([node_to_c_str(elt.value, scope) for elt in elements])}}}"

        # Catch-all error
        case _:
            raise TranspileError(SyntaxError(f"No support for {type(node)}"), node)

#ifndef DICT_HPP
#define DICT_HPP

#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "new.hpp"
#include "str.hpp"

#include "initializer_list.hpp"

#define EMPTY_INDEX ((1 << bits) - 1)
#define MAX(A, B) ((A > B) ? A : B)
#define CAPACITY (3 << (this->indicesLog2 - 2))

constexpr unsigned char calculateBits(unsigned char indicesLog2) noexcept {
  return indicesLog2 <= 8 ? 8
                          : (indicesLog2 <= 16   ? 16
                             : indicesLog2 <= 32 ? 32
                                                 : 64);
}

template <typename N> constexpr N log2(N n) noexcept {
  return (n > 1) ? 1 + log2(n >> 1) : 0;
}

template <typename K, typename V> struct DictEntry {
  uint32_t hash;
  K key;
  V value;
};

class Hash {
private:
  static uint32_t rotl32(uint32_t x, int8_t r) {
    return (x << r) | (x >> (32 - r));
  }
  static uint32_t fmix32(uint32_t h) {
    h ^= h >> 16;
    h *= 0x85ebca6b;
    h ^= h >> 13;
    h *= 0xc2b2ae35;
    h ^= h >> 16;

    return h;
  }

  static uint32_t murmur(const void *key, int len) {
    const uint8_t *data = (const uint8_t *)key;
    const int nblocks = len / 4;

    uint32_t h1 = 0;

    const uint32_t c1 = 0xcc9e2d51;
    const uint32_t c2 = 0x1b873593;

    //----------
    // body

    const uint32_t *blocks = (const uint32_t *)(data + nblocks * 4);

    for (int i = -nblocks; i; i++) {
      uint32_t k1 = blocks[i];

      k1 *= c1;
      k1 = rotl32(k1, 15);
      k1 *= c2;

      h1 ^= k1;
      h1 = rotl32(h1, 13);
      h1 = h1 * 5 + 0xe6546b64;
    }

    //----------
    // tail

    const uint8_t *tail = (const uint8_t *)(data + nblocks * 4);

    uint32_t k1 = 0;

    switch (len & 3) {
    case 3:
      k1 ^= tail[2] << 16;
    case 2:
      k1 ^= tail[1] << 8;
    case 1:
      k1 ^= tail[0];
      k1 *= c1;
      k1 = rotl32(k1, 15);
      k1 *= c2;
      h1 ^= k1;
    };

    //----------
    // finalization

    h1 ^= len;

    return fmix32(h1);
  }

public:
  static uint32_t hash(uint64_t hashable) { return hashable; }
  static uint32_t hash(String hashable) {
    return murmur(hashable.c_str(), hashable.length());
  }
};

template <typename K, typename V> class Dict {
private:
  unsigned char indicesLog2;
  void *indices;            // MaxSizeType[1 << indicesLog2]{EMPTY_INDEX}
  DictEntry<K, V> *entries; // DictEntry<K, V>[capacity]
  void *size;               // MaxSizeType

  void init() {
    unsigned char bits = calculateBits(this->indicesLog2);
    this->entries = (DictEntry<K, V> *)malloc(sizeof(DictEntry<K, V>) * CAPACITY);

    switch (bits) {
    case 8: // uint8_t
      this->indices = new uint8_t[1 << this->indicesLog2]{(uint8_t)EMPTY_INDEX};
      this->size = new uint8_t(0);
      break;
    case 16: // uint16_t
      this->indices = new uint16_t[1 << this->indicesLog2]{(uint16_t)EMPTY_INDEX};
      this->size = new uint16_t(0);
      break;
    case 32: // uint32_t
      this->indices =
          new uint32_t[1 << this->indicesLog2]{(uint32_t)EMPTY_INDEX};
      this->size = new uint32_t(0);
      break;
    case 64: // uint64_t
      this->indices =
          new uint64_t[1 << this->indicesLog2]{(uint64_t)EMPTY_INDEX};
      this->size = new uint64_t(0);
      break;
    }
  }

  void resize() {
    unsigned char bits = calculateBits(this->indicesLog2 + 1);

    // If we're resizing, assume we're at capacity; if we're increasing the
    // number of bits, re-allocate size
    if (bits != calculateBits(this->indicesLog2)) {
      switch (bits) {
      case 8: // uint8_t
        free((uint8_t *)indices);
        break;
      case 16: // uint16_t
        free((uint8_t *)this->size);
        this->size = new uint16_t(CAPACITY);
        free((uint16_t *)indices);
        break;
      case 32: // uint32_t
        free((uint16_t *)this->size);
        this->size = new uint32_t(CAPACITY);
        free((uint32_t *)indices);
        break;
      case 64: // uint64_t
        free((uint32_t *)this->size);
        this->size = new uint64_t(CAPACITY);
        free((uint64_t *)indices);
        break;
      }
    }

    uint64_t size = CAPACITY;

    this->indicesLog2++;

    DictEntry<K, V> *oldEntries = this->entries;
    this->entries = (DictEntry<K, V> *)malloc(sizeof(DictEntry<K, V>) * CAPACITY);
    memcpy((DictEntry<K, V> *)this->entries, oldEntries,
            size * sizeof(DictEntry<K, V>));
    free(oldEntries);

    if (bits == 8) { // uint8_t
      this->indices = new uint8_t[1 << this->indicesLog2]{(uint8_t)EMPTY_INDEX};

      DictEntry<K, V> entry;
      uint8_t maskedHash;
      for (uint8_t i = 0; i < size; i++) {
        entry = this->entries[i];
        maskedHash = entry.hash & ((1 << this->indicesLog2) - 1);

        if (((uint8_t *)this->indices)[maskedHash] == (uint8_t)EMPTY_INDEX)
          ((uint8_t *)this->indices)[maskedHash] = i;
      }
    } else if (bits == 16) { // uint16_t
      this->indices =
          new uint16_t[1 << this->indicesLog2]{(uint16_t)EMPTY_INDEX};

      DictEntry<K, V> entry;
      uint16_t maskedHash;
      for (uint16_t i = 0; i < size; i++) {
        entry = this->entries[i];
        maskedHash = entry.hash & ((1 << this->indicesLog2) - 1);

        if (((uint16_t *)this->indices)[maskedHash] == (uint16_t)EMPTY_INDEX)
          ((uint16_t *)this->indices)[maskedHash] = i;
      }
    } else if (bits == 32) { // uint32_t
      this->indices =
          new uint32_t[1 << this->indicesLog2]{(uint32_t)EMPTY_INDEX};

      DictEntry<K, V> entry;
      uint32_t maskedHash;
      for (uint32_t i = 0; i < size; i++) {
        entry = this->entries[i];
        maskedHash = entry.hash & ((1 << this->indicesLog2) - 1);

        if (((uint32_t *)this->indices)[maskedHash] == (uint32_t)EMPTY_INDEX)
          ((uint32_t *)this->indices)[maskedHash] = i;
      }
    } else if (bits == 64) { // uint64_t
      this->indices =
          new uint64_t[1 << this->indicesLog2]{(uint64_t)EMPTY_INDEX};

      DictEntry<K, V> entry;
      uint64_t maskedHash;
      for (uint64_t i = 0; i < size; i++) {
        entry = this->entries[i];
        maskedHash = entry.hash & ((1 << this->indicesLog2) - 1);

        if (((uint64_t *)this->indices)[maskedHash] == (uint64_t)EMPTY_INDEX)
          ((uint64_t *)this->indices)[maskedHash] = i;
      }
    }
  }

  V &getOrSet(K key, uint32_t hash, uint32_t maskedHash) {
    unsigned char bits = calculateBits(this->indicesLog2);

    uint64_t size, index;
    switch (bits) {
    case 8:
      size = *(uint8_t *)this->size;
      index = ((uint8_t *)this->indices)[maskedHash];
      break;
    case 16:
      size = *(uint16_t *)this->size;
      index = ((uint16_t *)this->indices)[maskedHash];
      break;
    case 32:
      size = *(uint32_t *)this->size;
      index = ((uint32_t *)this->indices)[maskedHash];
      break;
    case 64:
      size = *(uint64_t *)this->size;
      index = ((uint64_t *)this->indices)[maskedHash];
      break;
    }

    if (index == (uint64_t)EMPTY_INDEX) {
      if (size == (uint64_t)CAPACITY) {
        this->resize();
        bits = calculateBits(this->indicesLog2);
      }

      this->entries[size] = {hash, key, V()};

      if (bits == 8) {
        ((uint8_t *)this->indices)[maskedHash] = (uint8_t)size;
        (*(uint8_t *)this->size)++;
      } else if (bits == 16) {
        ((uint16_t *)this->indices)[maskedHash] = (uint16_t)size;
        (*(uint16_t *)this->size)++;
      } else if (bits == 32) {
        ((uint32_t *)this->indices)[maskedHash] = (uint32_t)size;
        (*(uint32_t *)this->size)++;
      } else if (bits == 64) {
        ((uint64_t *)this->indices)[maskedHash] = (uint64_t)size;
        (*(uint64_t *)this->size)++;
      }

      return this->entries[size].value;
    }

    if (bits == 8) {
      for (uint8_t i = ((uint8_t *)this->indices)[maskedHash]; i < size; i++) {
        if (this->entries[i].key == key)
          return this->entries[i].value;
      }
    } else if (bits == 16) {
      for (uint16_t i = ((uint16_t *)this->indices)[maskedHash]; i < size;
           i++) {
        if (this->entries[i].key == key)
          return this->entries[i].value;
      }
    } else if (bits == 32) {
      for (uint32_t i = ((uint32_t *)this->indices)[maskedHash]; i < size;
           i++) {
        if (this->entries[i].key == key)
          return this->entries[i].value;
      }
    } else if (bits == 64) {
      for (uint64_t i = ((uint64_t *)this->indices)[maskedHash]; i < size;
           i++) {
        if (this->entries[i].key == key)
          return this->entries[i].value;
      }
    }

    if (size == (uint64_t)CAPACITY) {
      this->resize();
      bits = calculateBits(this->indicesLog2);
    }

    this->entries[size] = {hash, key, V()};

    if (bits == 8)
      (*(uint8_t *)this->size)++;
    else if (bits == 16)
      (*(uint16_t *)this->size)++;
    else if (bits == 32)
      (*(uint32_t *)this->size)++;
    else if (bits == 64)
      (*(uint64_t *)this->size)++;

    return this->entries[size].value;
  }

  void add(K key, V value) {
    unsigned char bits = calculateBits(this->indicesLog2);

    uint64_t size;
    switch (bits) {
    case 8:
      size = *(uint8_t *)this->size;
      break;
    case 16:
      size = *(uint16_t *)this->size;
      break;
    case 32:
      size = *(uint32_t *)this->size;
      break;
    case 64:
      size = *(uint64_t *)this->size;
      break;
    }

    if (size == (uint64_t)CAPACITY) {
      this->resize();
      bits = calculateBits(this->indicesLog2);
    }

    uint32_t hash = Hash::hash(key);
    this->entries[size] = {hash, key, value};

    uint32_t maskedHash = hash & ((1 << this->indicesLog2) - 1);

    switch (bits) {
    case 8:
      if (((uint8_t *)this->indices)[maskedHash] == (uint8_t)EMPTY_INDEX)
        ((uint8_t *)this->indices)[maskedHash] = size;

      (*(uint8_t *)this->size)++;
      break;
    case 16:
      if (((uint16_t *)this->indices)[maskedHash] == (uint16_t)EMPTY_INDEX)
        ((uint16_t *)this->indices)[maskedHash] = size;

      (*(uint16_t *)this->size)++;
      break;
    case 32:
      if (((uint32_t *)this->indices)[maskedHash] == (uint32_t)EMPTY_INDEX)
        ((uint32_t *)this->indices)[maskedHash] = size;

      (*(uint32_t *)this->size)++;
      break;
    case 64:
      if (((uint64_t *)this->indices)[maskedHash] == (uint64_t)EMPTY_INDEX)
        ((uint64_t *)this->indices)[maskedHash] = size;

      (*(uint64_t *)this->size)++;
      break;
    }
  }

public:
  Dict(unsigned char indicesLog2 = 3) : indicesLog2(indicesLog2) {
    this->init();
  }

  Dict(std::initializer_list<K> keys, std::initializer_list<V> values,
       unsigned char indicesLog2 = 3) {
    size_t length = keys.size();
    if (length == 0 || length != values.size())
      return;

    this->indicesLog2 = MAX(MAX(log2(keys.size()), 3), indicesLog2);

    if (length > (uint64_t)CAPACITY)
      return;

    this->init();

    auto key_iter = keys.begin();
    auto val_iter = values.begin();
    while (key_iter != keys.end()) {
      this->add(*key_iter, *val_iter);

      key_iter++;
      val_iter++;
    }
  }

  V &operator[](K key) {
    uint32_t hash = Hash::hash(key);
    uint32_t maskedHash = hash & ((1 << this->indicesLog2) - 1);

    return this->getOrSet(key, hash, maskedHash);
  }

  inline void *length() {
    return this->size;
  }

  uint64_t friendlyLength() {
    switch (calculateBits(this->indicesLog2)) {
    case 8:
        return *(uint8_t *)this->size;
    case 16:
        return *(uint16_t *)this->size;
    case 32:
        return *(uint32_t *)this->size;
    case 64:
        return *(uint64_t *)this->size;
    }
  }

  inline unsigned char bits() {
    return calculateBits(this->indicesLog2);
  }

  template <typename N> inline K keys(N i) {
    return this->entries[i].key;
  }
};


#endif // DICT_HPP

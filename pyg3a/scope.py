#!/usr/bin/env python3
import types
from collections.abc import Mapping
from pathlib import Path
from types import EllipsisType
from typing import (
    Any,
    Final,
    Never,
    Optional,
    Sequence,
    cast,
    Self,
    NamedTuple,
    Iterable,
    Iterator,
    override,
    Union,
    Collection,
)

import libcst as cst

import pyg3a
import pyg3a.modules as modules
from pyg3a.types import Types


class NoValueT:
    """
    Singleton class representing no set value in a :py:class:`Scope` or :py:class:`modules.functions.Parameter`.
    """

    @override
    def __repr__(self) -> str:
        return "NoValue"

    def __bool__(self) -> bool:
        """
        Empty value (like None) treated as False-y.
        """

        return False

    def __or__[T: type](self, other: T) -> Union[type[Self], T]:
        """
        Allowing typing Unions with other classes
        """

        return Union[type(self), other]

    def __ror__[T: type](self, other: T) -> Union[T, type[Self]]:
        """
        Allowing typing Unions with other classes
        """

        return Union[other, type(self)]


NoValue: NoValueT = NoValueT()


class VariableInfo(NamedTuple):
    """
    Tuple representing a variable's type and value in a :py:class:`Scope`.
    """

    type: Types.type | type[Types.type] | type
    "The type of a variable or function."

    value: Any | NoValueT = NoValue
    "The value of a variable or function, or NoValue if it is unknown/not set."


def _get_called_func_name(node: "modules.FuncTypes", scope: "Scope") -> str:
    from pyg3a.node import node_to_c_str

    if isinstance(node, cst.Call):
        return node_to_c_str(node.func, scope)

    return node_to_c_str(node.iter.func, scope)


class Scope(Iterable[tuple[str, VariableInfo]]):
    """
    A high-level mapping of variable or function names to their types and values.
    """

    _data: Final[dict[str, VariableInfo]]
    "Internal mapping of variable names to their types and values."

    _parent: Final[Optional["Scope"]]
    "Optional (mutable) 'parent' scope, used to look up variables when not found in :py:attr:`_data`."

    _modules: Final["ModuleSet"]
    "Set of modules this scope has imported."

    __slots__ = "_data", "_parent", "_modules"

    def __init__(
        self,
        parent: Optional[Self] = None,
        mapping: Optional[Mapping[str, VariableInfo | tuple[Types.type | type[Types.type], Any]]] = None,
        /,
        **kwargs: VariableInfo | tuple[Types.type | type[Types.type], Any],
    ) -> None:
        """
        Create a new scope from a mapping or kwargs, with optional parent.

        :param parent: Optional parent scope.
        :param mapping: Optional mapping of ``variable_name`` -> :py:class:`VariableInfo` or ``tuple(type, value)``.
        :param kwargs: Optional kwargs used as well as or instead of ``mapping``.
        """

        self._data = {}
        self._parent = parent
        self._modules = ModuleSet(self)

        if mapping is not None:
            for key in mapping:
                if isinstance(mapping[key], VariableInfo):
                    self._data[key] = mapping[key]
                else:
                    self._data[key] = VariableInfo(*mapping[key])
        if kwargs:
            for key, value in kwargs.items():
                if isinstance(value, VariableInfo):
                    self._data[key] = value
                else:
                    self._data[key] = VariableInfo(*value)

    def copy(self) -> "Scope":
        """
        Create a deep copy of this scope without changing the parent scope.
        """
        return Scope(self._parent, self._data.copy())

    def __getitem__(self, var: str | cst.Name) -> VariableInfo:
        """
        Get the type and value of a variable.

        :param var: The name of the variable or a :py:class:`cst.Name` node representing it.
        :returns: The type and value of the variable.
        :raises KeyError: If the variable is not found.
        """

        if (isinstance(var, str) and var in self._data) or (isinstance(var, cst.Name) and var.value in self._data):
            return self._data[var] if isinstance(var, str) else self._data[var.value]

        if self._parent and var in self._parent:
            return self._parent[var]

        raise KeyError

    def __contains__(self, var: str | cst.Name) -> bool:
        """
        Check if a variable is defined in this or the parent scope.
        If this returns ``True``, then :py:meth:`__getitem__` will return the type and value of the variable, \
        else :py:meth:`__getitem__` will raise a :py:class:`KeyError`.

        :param var: The name of the variable or a :py:class:`cst.Name` node representing it.
        :returns: ``True`` if the variable is defined in this or the parent scope, else ``False``.
        """
        return (
            (var in self._data)
            if isinstance(var, str)
            else (var.value in self._data if isinstance(var, cst.Name) else False)
        ) or (var in self._parent if self._parent else False)

    def __setitem__(self, *_: Any) -> Never:
        raise TypeError("Use set_var, set_value, or set_type")

    def __repr__(self) -> str:
        return f"Scope(parent={self._parent!r}, modules={self._modules!r}, mapping={self._data!r})"

    def __str__(self) -> str:
        return f"Scope(\n      parent={"\n      ".join(str(self._parent).split("\n"))},\n      modules={self._modules},\n      {", \n      ".join([f"{name}: ({repr(typ)}, {repr(val)})" for name, (typ, val) in self._data.items()]) if self._data else "<no data>"}\n)"

    @override
    def __iter__(self) -> Iterator[tuple[str, VariableInfo]]:
        """
        Get iterator over internal variable_name -> variable_info mapping

        :returns: dict_items iterator of internal :py:attr:`_data` attribute.
        """

        for pair in self._data.items():
            yield pair

        if self._parent:
            for pair in self._parent:
                yield pair

    @property
    def parent(self) -> "Scope":
        """
        Get this scope's parent, if it has one.

        :returns: The parent scope (or itself if there is no parent).
        """

        if self._parent:
            return self._parent

        return self

    def set_var(self, var: cst.Name, typ: Types.type | type[Types.type], value: Any | NoValueT) -> None:
        """
        Set the type and value of a variable.

        :param var: The name of the variable or a :py:class:`cst.Name` node representing it.
        :param typ: The type of the variable.
        :param value: The value of the variable.
        :raises TypeError: If the variable name is of the wrong type, the variable name is empty, or the type is not a type object.
        """

        if not isinstance(var, cst.Name):
            raise TypeError("Variable name must be a libcst.Name node.")

        if not (
            isinstance(typ, Types.type)
            or (
                isinstance(typ, type)
                and issubclass(typ, Types.type)
                and (isinstance(value, Types.type) or value is NoValue)
            )
            or (issubclass(typ, type) and (issubclass(value, Types.type) or value is NoValue))
            or (
                isinstance(typ, type)
                and issubclass(typ, types.FunctionType)
                and (isinstance(value, types.FunctionType) or value is NoValue)
            )
        ):
            raise TypeError("Passed type is not a type object or class.")

        self._data[var.value] = VariableInfo(typ, value)

    def set_func(self, name: str, param_types: Collection[Types.type] | EllipsisType, return_type: Types.type) -> None:
        """
        Set the parameter and return types of a function.

        :param name: The name of the function.
        :param param_types: The types of the parameters.
        :param return_type: The type of the return value.
        :raises TypeError: If the function name is empty.
        """

        if not name:
            raise TypeError("Function name empty.")

        if not (
            param_types is ...
            or len(param_types) == 0
            or (isinstance(param_types, Iterable) and all(isinstance(param, Types.type) for param in param_types))
        ):
            raise TypeError("Invalid parameter types.")

        if not isinstance(return_type, Types.type):
            raise TypeError("Return type is not a type object.")

        self._data[name] = VariableInfo(
            Types.Callable[... if param_types is Ellipsis else list(param_types), return_type]
        )

    def set_value(self, var: cst.Name, value: Any | NoValueT) -> None:
        """
        Set the value of a variable.

        :param var: The name of the variable or a :py:class:`cst.Name` node representing it.
        :param value: The value of the variable.
        :raises TypeError: If the variable name is empty or the variable is not already defined in this scope.
        """

        if not isinstance(var, cst.Name):
            raise TypeError("Variable name must be a libcst.Name node.")

        if var.value not in self._data:
            raise TypeError("Type unspecified - use set_var or set_type")

        self._data[var.value] = VariableInfo(self._data[var.value].type, value)

    def set_type(self, var: cst.Name, typ: Types.type | type[Types.type]) -> None:
        """
        Set the type of a variable.

        :param var: The name of the variable or a :py:class:`cst.Name` node representing it.
        :param typ: The type of the variable.
        :raises TypeError: If the variable name is of the wrong type, the variable name is empty, or the type is not a type object.
        """

        if not isinstance(var, cst.Name):
            raise TypeError("Variable name must be a libcst.Name node.")

        if not (isinstance(typ, Types.type) or (isinstance(typ, type) and issubclass(typ, Types.type))):
            raise TypeError("Passed type is not a type object or class.")

        self._data[var.value] = VariableInfo(typ, self._data[var.value].value if var.value in self._data else NoValue)

    def import_module(self, module_name: str, names: set[str] | None = None) -> list[str]:
        """
        Import a module into the scope.

        :param module_name: The name of the module.
        :param names: An optional set of (function, variable, class, etc.) names to import.
        :raises ImportError: If the module is not found.
        """

        # Find module_name.py in Main.package_locs
        # Final location has priority
        file_path: Path = Path()
        for loc in pyg3a.Main.package_locs:
            if loc.joinpath(f"{module_name}.py").is_file():
                file_path = loc.joinpath(f"{module_name}.py")

        # If module doesn't exist, raise error
        if not file_path.is_file():
            raise ModuleNotFoundError(f"No module named '{module_name}'", name=module_name, path=str(file_path))

        self._modules.add(modules.Module(module_name, self).parse_file(file_path), names if names else set())

        try:
            return self._modules.get_import_statements(module_name)
        except KeyError:
            return []

    def pretty_modules_str(self) -> str:
        """
        Create a string representation of the modules in this scope.

        :returns: The string representation.
        """

        return str(self._modules)

    def has_conversion_for(self, node: "modules.FuncTypes", /, scope: "Scope | None" = None) -> bool:
        if self._modules.contains(node, scope if scope else self):
            return True

        if self._parent:
            return self._parent.has_conversion_for(node, scope=(scope if scope else self))

        return False

    def convert_from(self, node: "modules.FuncTypes", /, scope: "Scope | None" = None, **kwargs: Any) -> str:
        try:
            if isinstance(node, cst.Call) and node.func.value == "PrintMini":
                pass
            return self._modules.convert(node, (scope if scope else self), **kwargs)
        except KeyError:
            if not self._parent:
                raise KeyError(node)

            return self._parent.convert_from(node, scope=(scope if scope else self), **kwargs)

    def get_conversion_return_type_for(
        self, node: "modules.FuncTypes", /, scope: "Scope | None" = None
    ) -> Types.type | None:
        try:
            return self._modules.returns(node, (scope if scope else self))
        except KeyError:
            if not self._parent:
                return None

            return self._parent.get_conversion_return_type_for(node, scope=(scope if scope else self))

    def inner(
        self,
        var: Optional[str | cst.Param | Sequence[str] | Sequence[cst.Param]] = None,
        typ: Optional[Types.type | type[Types.type] | Sequence[Types.type | type[Types.type]]] = None,
    ) -> "Scope":
        """
        Create a new scope that is a child of this scope.
        Optionally, one or more variables' types can be set.
        If a sequence is passed to ``var`` or ``typ``, a sequence of the same length should be passed to the other.
        The parent will remain unchanged.

        :param var: Optional name of a new variable to set the type of, or a :py:class:`cst.Param` node representing it, \
        or a sequence of one of these.
        :param typ: Optional type of a new variable to set, or a sequence of types the same length as ``var``.
        :returns: The child scope.
        """
        if not var or not typ:
            return Scope(self)

        new_scope: Scope = Scope(self)
        variables: list[str]

        match var:
            case str():
                variables = [var]
            case cst.Param():
                variables = [var.name.value]
            case [*seq] if isinstance(seq[0], cst.Param):
                variables = [param.name.value for param in cast(Sequence[cst.Param], var)]
            case _:
                variables = list(cast(Sequence[str], var))

        if isinstance(typ, Sequence):
            for i in range(min(len(variables), len(typ))):
                new_scope._data[variables[i]] = VariableInfo(typ[i])
        else:
            for i in range(len(variables)):
                new_scope._data[variables[i]] = VariableInfo(typ)

        return new_scope


class ModuleSet:
    """
    A high-level wrapper allowing users to access interfaces on all modules in a set.
    """

    _funcs: Final[dict[type["modules.FuncTypes"], dict[str, dict["modules.Function", None]]]]
    "Mapping of function types to functions of that type within the added modules."
    _names: Final[set[str]]
    "Set of names of added modules."
    _main_funcs: Final[dict[str, "modules.Function"]]
    "List of main (run-on-import) functions of added modules."
    _scope: Scope
    "The scope in which modules are imported into."

    __slots__ = "_funcs", "_names", "_main_funcs", "_scope"

    @override
    def __init__(self, scope: Scope) -> None:
        """
        Create a new ModuleSet in a scope from an optional iterable of modules.

        :param scope: The scope in which modules are imported into.
        """

        # Generate empty dict for each function type by default
        self._funcs = {node: {} for node in modules.FuncTypes.__args__}
        self._names = set()
        self._main_funcs = {}
        self._scope = scope
        #
        # # If an iterable is passed, add its module names to our set and populate the function sets
        # for module in iterable:
        #     self._names.add(module.name)
        #
        #     if module.main_func:
        #         self._main_funcs.append(module.main_func)
        #
        #     for function in module.functions:
        #         if function.name in self._funcs[function.typ]:
        #             self._funcs[function.typ][function.name][function] = None
        #         else:
        #             self._funcs[function.typ][function.name] = {function: None}

    @override
    def __repr__(self) -> str:
        return f"ModuleSet({', '.join(self._names)})"

    @override
    def __str__(self) -> str:
        return f"{{{', '.join(self._names)}}}"

    # def __contains__(self, module: str) -> bool:
    #     """
    #     Determine whether the specified module is contained within this set.
    #
    #     :param module: The name of a module to check.
    #     :returns: True if the module is contained within this set, False otherwise.
    #     """
    #
    #     return module in self._names
    #

    # TODO: Refactor to remove names here and import whole modules then in Scope we can filter and thus save time when re-importing modules?
    def add(self, module: "modules.Module", names: set[str]) -> None:
        """
        Add a module to the set.

        :param module: The :py:class:`~modules.modules.Module` to add.
        :param names: The set of (function, variable, class, etc.) names to add to the set, or empty for all.
        """

        self._names.add(module.name)

        if module.main_func:
            self._main_funcs[module.name] = module.main_func

        for function in module.functions:
            if names and function.name not in names:
                continue

            if function.name in self._funcs[function.accepted_node]:
                self._funcs[function.accepted_node][function.name][function] = None
            else:
                self._funcs[function.accepted_node][function.name] = {function: None}

        if names:
            for name in names:
                if name in module.scope:
                    self._scope.set_var(cst.Name(value=name), module.scope[name].type, module.scope[name].value)
        else:
            for name, (typ, val) in module.scope:
                self._scope.set_var(cst.Name(value=name), typ, val)

    def contains(self, node: "modules.FuncTypes", scope: Scope) -> bool:
        """
        Determine whether this set provides a conversion for a specified node.

        :param node: The node to check. See :py:class:`~modules.functions.Function` for FuncTypes.
        :param scope: The node's enclosing scope.
        :returns: True if a conversion exists, False if it doesn't.
        """

        for fun in self._funcs[type(node)].get(_get_called_func_name(node, scope), tuple()):
            if fun.accepts(node, scope):
                return True
        return False

    def convert(self, node: "modules.FuncTypes", scope: Scope, **kwargs: Any) -> str:
        """
        Convert a node to its C string using this set's function converters.

        :param node: The node to convert. See :py:class:`~modules.functions.Function` for FuncTypes.
        :param scope: The node's enclosing scope.
        :param kwargs: Additional arguments to be passed to the converter. See \
        :py:class:`~modules.functions.FunctionInstance` for possible arguments.
        :returns: C string representation of the node.
        :raises KeyError: If a conversion does not exist within this set.
        """

        for fun in self._funcs[type(node)].get(_get_called_func_name(node, scope), tuple()):
            if inst := fun.accepts(node, scope):
                return inst.convert(**kwargs)
        raise KeyError(node)

    def returns(self, node: "modules.FuncTypes", scope: Scope) -> Types.type:
        """
        Get the return type of a :py:class:`~modules.functions.Function` from this set.

        :param node: The node to check. See :py:class:`~modules.functions.Function` for FuncTypes.
        :param scope: The node's enclosing scope.
        :returns: The type this function returns.
        :raises KeyError: If a conversion does not exist within this set.
        """

        for fun in self._funcs[type(node)].get(_get_called_func_name(node, scope), tuple()):
            if inst := fun.accepts(node, scope):
                inst.update_return()
                return inst.return_type

        raise KeyError(node)

    def get_import_statements(self, module_name: str) -> list[str]:
        """
        Get the statements run when importing this set.

        :param module_name: The name of the module to get import statements for.
        :returns: An iterable of strings containing the import statements for this set.
        """

        return (
            modules.FunctionInstanceCall(
                self._main_funcs[module_name],
                cst.Call(func=cst.Name(value="__main__")),
                self._scope,
                Types.NoneType,
            )
            .eval_module_func()
            .split("\n")
        )

import stdlib


def __main__() -> None:
    import fxcg.rtc

    return "srand(RTC_GetTicks())"


def seed(s: int = 1) -> None:
    return f"srand({s})"


# randrange(x, y?, z?) == choice(range(x, y?, z?))
def randrange(
    stop: int,
) -> int:
    return f"(((unsigned long) rand() * {stop}) / 32767)"


def randrange(start: int, stop: int) -> int:
    return f"(((unsigned long) rand() * ({stop} - {start})) / 32767 + {start})"


def randrange(start: int, stop: int, step: int = 1) -> int:
    return f"(((unsigned long) rand() * (({stop} - {start} + {step} - 1) / {step})) / 32767 * {step} + {start})"


# randint(a, b) == randrange(a, b+1) == [a, b]
def randint(a: int, b: int) -> int:
    from . import randrange

    return randrange(a, b + 1)


# getrandbits(k) == randrange(2**k) == [0, 2**k - 1]
# Undefined for k < 0
def getrandbits(k: int) -> int:
    return f"(({k} == 0) ? 0 : (((unsigned long) rand() * (1 << ({k} - 1))) / 32767))"


# Random item of seq
def choice[T](seq: Sequence[T]) -> T:
    from . import randrange
    from . import len as __pyg3a_len

    return f"{seq}[{randrange(__pyg3a_len(seq))}]"


# Random float a -> b
def uniform(a: float, b: float) -> float:
    return f"(({a} > {b}) ? (((double) rand() / 32767.0 * ({b} - {a})) + {a}) : (((double) rand() / 32767.0 * ({a} - {b})) + {b}))"


# random() == uniform(0.0, 1.0)
def random() -> float:
    return "((double) rand() / 32767.0)"

# Normal distribution - mean mu, standard deviation sigma
def gauss(mu: float = 0.0, sigma: float = 1.0) -> float:
    import math

    @c_func
    def sqrt(n: float) -> float:
        return """
      // Max and min are used to take into account numbers less than 1
      double lo, hi, mid;
      if (1 < n) {
        lo = 1;
        hi = n;
      } else {
        lo = 1;
        hi = n;
      }

      // Update the bounds to be off the target by a factor of 10
      while(100 * lo * lo < n) lo *= 10;
      while(0.01 * hi * hi > n) hi *= 0.1;

      for(int i = 0 ; i < 100 ; i++){
        mid = (lo+hi)/2;
        if(mid*mid == n) return mid;
        if(mid*mid > n) hi = mid;
        else lo = mid;
      }
      return mid;
        """

    @c_func
    def ln(x: float) -> float:
        import stdint

        return """
          uint32_t bx = * (uint32_t *) (&x);
          uint32_t ex = bx >> 23;
          int32_t t = (int32_t)ex-(int32_t)127;
          uint32_t s = (t < 0) ? (-t) : t;
          bx = 1065353216 | (bx & 8388607);
          x = * (float *) (&bx);
          return -1.49278+(2.11263+(-0.729104+0.10969*x)*x)*x+0.6931471806*t;
        """

    return f"({mu} + ({sqrt(f"-2 * {log("(double) rand() / 32767.0")}")} * {sigma}))"

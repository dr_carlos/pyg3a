#!/usr/bin/env python3


def len(s: str) -> int:
    import string

    return f"strlen({s}.c_str())"


def len(l: list[Any]) -> int:
    return f"{l}.length()"


def len[T](l: tuple[T, ...]) -> int:
    return f"List<{T}>{l}.length()"


def str(o: int) -> str:
    import fxcg.misc

    @c_func
    def str(i: int) -> "str":
        return """
        unsigned char buffer[12];
        itoa(i, buffer);
        return String((char *) buffer);
        """

    return str(o)


def str(o: str) -> str:
    return o


def range(stop: int) -> tuple[int, ...]:
    return f"{{{', '.join([str(i) for i in range(int(stop))])}}}"


def range(start: int, stop: int, step: int = 1) -> tuple[int, ...]:
    return f"{{{', '.join([str(i) for i in range(int(start), int(stop), int(step))])}}}"


def range__iter__(var_name: str, stop: int) -> int:
    return f"for (int {var_name} = 0; {var_name} < {stop}; {var_name}++) {{"


def range__iter__(var_name: str, start: int, stop: int, step: int = 1) -> int:
    return f"for (int {var_name} = {start}; {var_name} < {stop}; {var_name} += {step}) {{"


def enumerate__iter__[T](var_names: tuple[str, ...], iterable: Sequence[T]) -> tuple[int, T]:
    from . import len as __pyg3a_len

    return f"for (int {var_names[0]} = 0; {var_names[0]} < {__pyg3a_len(iterable)}; {var_names[0]}++) {{\n\t{T} {var_names[1]} = {iterable}[{var_names[0]}];"


def int(number: float) -> int:
    return f"(int) ({number})"


def round(number: float) -> int:
    @c_func
    def round(val: float) -> int:
        return """
        if (val < 0.0)
            return (int) (val - 0.5);
        return (int) (val + 0.5);
        """

    return round(number)


def max(a: int, b: int) -> int:
    @c_func
    def max(a: int, b: int) -> int:
        return """
        if (a > b)
            return a;
        return b;
        """

    return max(a, b)


def min(a: int, b: int) -> int:
    @c_func
    def min(a: int, b: int) -> int:
        return """
        if (a < b)
            return a;
        return b;
        """

    return min(a, b)


def __pyg3a_sprintf() -> str:
    import stdarg
    import stdio

    @c_func
    def sprintf(fmt: str, *args: Any) -> str:
        return """
        char buffer[256];
        sprintf(buffer, fmt.c_str(), args...);
        return String(buffer);
        """

    return sprintf()[:-2]


def __pyg3a_contains[T](elem: T, seq: tuple[T, ...]) -> bool:
    @c_func
    def contains(e: T, s: tuple[T, ...], len: int) -> bool:
        return f"""
        for (int i = 0; i < len; i++)
            if (s[i] == e)
                return true;
        return false;
        """

    from . import len as __pyg3a_len

    return contains(elem, seq, __pyg3a_len(seq))


def __pyg3a_contains[T](elem: T, seq: list[T]) -> bool:
    @c_func
    def contains(seq: list[T], elem: T) -> bool:
        return """
        for (int i = 0; i < seq.length(); i++)
            if (seq[i] == elem)
                return true;
        return false;
        """

    return contains(seq, elem)


def __pyg3a_contains[K, V](elem: K, seq: dict[K, V]) -> bool:
    @c_func
    def contains[W](dic: dict[K, W], elem: K) -> bool:
        return """
        unsigned char bits = dic.bits();
        
        if (bits == 8) {
            for (uint8_t i = 0; i < *(uint8_t *)(dic.length()); i++)
                if (dic.keys(i) == elem)
                    return true;
        } else if (bits == 16) {
            for (uint16_t i = 0; i < *(uint16_t *)(dic.length()); i++)
                if (dic.keys(i) == elem)
                    return true;
        } else if (bits == 32) {
            for (uint32_t i = 0; i < *(uint32_t *)(dic.length()); i++)
                if (dic.keys(i) == elem)
                    return true;
        } else {
            for (uint64_t i = 0; i < *(uint64_t *)(dic.length()); i++)
                if (dic.keys(i) == elem)
                    return true;
        }
        
        return false;
        """

    return contains(seq, elem)


def print(
    *values: object,
    sep: str = 'String(" ")',
    end: str = r'String("\n")',
) -> None:
    import fxcg.display
    import initializer_list

    @syscall(0x1EF)
    def GetPrintMiniPos(
        message: "const char *",
    ) -> None: ...

    global __cursor_x
    __cursor_x = 0

    global __cursor_y
    __cursor_y = 0

    @c_func
    def print(value: str) -> None:
        return """
        String new_value = value.replace('\\\\n', 0);
        PrintMini(&__cursor_x, &__cursor_y, new_value.c_str(), 0x02, 0xFFFFFFFF, 0, 0, 0, 0, 1, 0);
        
        // If the replaced value was different, or we need to wrap the text, add a linebreak
        if (new_value != value || __cursor_x > 216) {
            __cursor_y += 18;
            __cursor_x = 0;
            
            // Wrap after exceeding screen height (warning: does not erase text underneath)
            // TODO: Scrollbars!
            if (__cursor_y > 344)
                __cursor_y = 0;
        }
        """

    from . import str as __pyg3a_str

    # I'm sure there's a more efficient string concatenation method here (do it in Python not C++)
    to_print: str = ""

    if values:
        to_print = __pyg3a_str(values[0])
        for i in range(len(values) - 1):
            to_print += (sep + __pyg3a_str(values[i + 1])) if sep != 'String("")' else __pyg3a_str(values[i + 1])

    if end != 'String("")':
        to_print += end

    if to_print:
        return print(to_print)

    raise RuntimeError("Empty print")

Welcome to PyG3A's documentation!
=================================

**PyG3A** (/ˈpai ˌdʒi: ˌθɹi: ˌei/) is a Python tool to convert Python files to Casio fx-CG 10/20/50 (Prizm) add-ins (G3A files).

Check out the :doc:`usage` section for further info.

.. note::

   This project is under active development.

.. toctree::
   :hidden:

   Home page <self>
   usage
   syntax
   about
   API Reference <_autosummary/pyg3a>

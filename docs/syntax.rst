Syntax
======

.. toctree::

Notes
-----

The function ``raw_c(str)`` inserts that str into the transpiled C++.

Importing replaces dots (``.``) with slashes (``/``), for example:
::

    import custom.random
    from custom.random import *
    from custom.random import a

Any of these lines would import the '``random``' module in the '``~/.local/lib/pyg3a/custom``' folder.
Variables are statically typed.
PyG3A will try and infer types, but it is recommended that you use type hints where possible. Function parameter types are not inferred and must be specified.

Syntax Support
--------------

===============  ===============  ================================================================================
    Keyword          Support                 Explanation
===============  ===============  ================================================================================
def              Supported
return           Supported
=                Supported
:=               Supported
(operator)=      Supported
a, _ = (a, b)    Supported
while            Supported
for              Supported
if               Supported
elif             Supported
else             Supported
import           Supported
from             Supported
pass             Supported
break            Supported
continue         Supported
lambda           Supported
a if b else c    Supported
a.b              Supported
[list]           Supported
and              Supported
or               Supported
\+               Supported
\-               Supported
\*               Supported
/                Supported
//               Supported
%                Supported
\*\*             Supported
<<               Supported
>>               Supported
\\               Supported
^                Supported
&                Supported
~                Supported
not              Supported
==               Supported
<                Supported
<=               Supported
>                Supported
\>=              Supported
is               Supported
is not           Supported
del              Supported
match            Supported
type             Supported
type generic[T]  Supported
\*args           Supported
in               Supported
not in           Supported
{di: ct}         Supported
(tuple)          Partial Support
{set}            Partial Support - used for structs
f"strings"       Partial Support  some f-string features are unsupported in libfxcg and others are not implemented
\*tuple          Support Planned
\*\*kwargs       Support Planned
args, \*, /, kw  Support Planned
class            Support Planned
[sli:ces]        Support Planned
a for b in c     Support Planned
@decorator       Support Planned
def func[T]      Support Planned
\*\*dict         Support Planned
assert           Support Planned
from import      Support Planned
import as        Support Planned
nonlocal         Support Planned
with             Support Planned
while / else     Support Planned
for / else       Support Planned
async            No Support       no async support in c++
await            No Support       no async support in c++
raise            No Support       no error support in libfxcg
try/except       No Support       no error support in libfxcg
global           No Support       automatically used in c++
yield            No Support       no generator support in c++
matrix @ matrix  No Support       little use case
===============  ===============  ================================================================================
